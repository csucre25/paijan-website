<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Formulario de reclamaciones</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
		<link href="css/tailwind.min.css" rel="stylesheet">
		<link href="css/style_1.css" rel="stylesheet">
		
		<style>
        input[type=radio]+label span {
            transition: background .2s, transform .2s
        }

        .fileContainer:hover {
            background-color: #ebb300
        }

        label.error {
            color: #f56565;
            font-size: .75rem;
            font-style: italic
        }

        input[type=radio]+label span:hover,
        input[type=radio]+label:hover span {
            transform: scale(1.2)
        }

        input[type=radio]:checked+label span {
            background-color: #4299e1;
            box-shadow: 0 0 0 2px #fff inset
        }

        input[type=radio]:checked+label {
            color: #4299e1
        }

        #loader {
            display: none
        }

        .fileContainer {
            overflow: hidden;
            position: relative;
        }

        .fileContainer [type=file] {
            cursor: inherit;
            display: block;
            font-size: 999px;
            filter: alpha(opacity=0);
            min-height: 100%;
            min-width: 100%;
            opacity: 0;
            position: absolute;
            right: 0;
            text-align: right;
            top: 0;
        }

        .fileContainer {
            background: #f5bd08;
            border-radius: .5em;
            float: left;
            padding: .5em;
            color: white;
            margin-bottom: 1rem;
        }

        .fileContainer [type=file] {
            cursor: pointer;
        }
    </style>
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 

		<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Formulario de reclamaciones</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Inicio</a></li>
						  <li class="breadcrumb-item active"><a href="mesapartes.php">Formulario de reclamaciones</a></li>						  
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			
			<!-- CITY SERVICES2 WRAP START-->
			<div class="city_health_department">
				<div class="container">
					<div class="row d-flex align-items-stretch no-gutters">
						<div class="col-md-6 p-4 p-md-5 order-md-last bg-light">
							<form action="#">
								<h2>Datos del Solicitante:</h2>
								<div class="form-group row">
									<div>Nombre Completo/Razón Social:</div>
										<div style="width : 510px;">
											<p><input type="text" class="form-control"></p>
										</div>
								</div>
								<div>Tipo de Documento:</div>
								<div class="form-group row">
									<div class="col-xs-6">
										<div class="input-group">
											<select class="form-control">
												<option>D.N.I.</option>
												<option>R.U.C.</option>
												<option>PASAPORTE</option>
											</select>
										</div>
									</div>

									<div class="col-xs-6">
										<div class="input-group">
											<input type="number" class="form-control input-lg" placeholder="Nro Documento:" required>
										</div>
									</div>
								</div>

								<div>Dirección:</div>
								<div class="form-group row">
									<input type="text" class="form-control" placeholder="" required>
								</div>
								<div>Teléfono:</div>
								<div class="form-group row">
									<input type="number" class="form-control" placeholder="" required>
								</div>
								<div>Email:</div>
								<div class="form-group row">
									<input type="email" class="form-control" placeholder="" required>
								</div>
								<h2>Datos del Reclamo:</h2>
								<div>Identificación del Área:</div>
								<div class="form-group row">
									<select class="form-control">
										<option>Seleccionar</option>
										<option value="ALCALDIA">ALCALDIA</option>
										<option value="ALMACEN">ALMACEN</option>
										<option value="CAMAL MUNICIPAL">CAMAL MUNICIPAL</option>
										<option value="COACTIVA">COACTIVA</option>
										<option value="EQUIPO MECANICO Y MAESTRAZA">EQUIPO MECANICO Y MAESTRAZA</option>
										<option value="GERENCIA DE ADMINISTRACION TRIBUTARIA">GERENCIA DE ADMINISTRACION TRIBUTARIA</option>
										<option value="GERENCIA DE ADMINISTRACION Y FINANZAS">GERENCIA DE ADMINISTRACION Y FINANZAS</option>
										<option value="GERENCIA DE ASESORIA JURIDICA">GERENCIA DE ASESORIA JURIDICA</option>
									</select>
								</div>
								<div>Descripción:</div>
								<div class="form-group row">
									<textarea name="" id="" cols="25" maxlength="1000" rows="6" class="form-control" placeholder=""></textarea>
								</div>
								<div>Adjuntar archivo (si es necesario):</div>
								<div class="form-group row">
									<input type="file" class="form-control" placeholder="" required>
								</div>
								
								<div class="form-group">
									<input type="submit" value="Enviar Mensaje" class="btn btn-primary py-3 px-5" onclick="pulsar()">
								</div>
							</form>
						</div>
						
					</div>
				</div>
	
			</div>
			<!-- CITY SERVICES2 WRAP END-->

			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->
            <?php include 'footer.php'; ?>
		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
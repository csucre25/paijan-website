<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Contact Us Baldiyaat - Local Government Template</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/selectric.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 
			<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Contáctanos</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Home</a></li>
						  <li class="breadcrumb-item active">Contáctanos</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			
			<!-- CITY EVENT2 WRAP START-->
			<div class="city_blog2_wrap team">
				<div class="container">
					<!--<div class="city_contact_map">
						<div id="map-canvas" class="map-canvas"></div>
					</div>-->
					<div class="city_contact_row">	
						<!--<div class="city_contact_list">
							<div class="row">
								<div class="col-md-6">
									<div class="city_contact_text">
										<h3>Comments, <br>Compliments and <br>Complaints</h3>
										<span><i class="fa icon-comment-1"></i></span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="city_contact_text">
										<h3>Contact with <br>us About our<br>Services</h3>
										<span><i class="fa icon-agenda"></i></span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="city_contact_text">
										<h3>Take A Part As <br>Consultant & <br>Voluntree</h3>
										<span><i class="fa icon-help"></i></span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="city_contact_text text2">
										<h3>Fellow Us on Social Media </h3>
										<div class="city_top_social">
											<ul>
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-youtube"></i></a></li>
												<li><a href="#"><i class="fa fa-google"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>-->
						<div class="city_event_detail contact">
							<div class="section_heading center">
								<span>Municipalidad de Paiján</span>
								<h2>Contacta con nosotros</h2>
							</div>
							<div class="event_booking_form">
								<div class="row">
									<div class="col-md-6">
										<div class="event_booking_field">
											<input type="text" placeholder="Nombres y apellidos">
										</div>
									</div>
									<div class="col-md-6">
										<div class="event_booking_field">
											<input type="text" placeholder="Correo electrónico">
										</div>
									</div>
									<div class="col-md-6">
										<div class="event_booking_field">
											<select class="small">
												<option data-display="Porfavor selecciona el servicio que requiere ">Porfavor selecciona el servicio que requiere </option>
												<option value="1">Servicio 1</option>
												<option value="2">Servicio 2</option>												
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="event_booking_field">
											<input type="text" placeholder="Asunto">
										</div>
									</div>
									<div class="col-md-12">
										<div class="event_booking_area">
											<textarea>Ingresa tu mensaque aquí</textarea>
										</div>
										<a class="theam_btn btn2" href="#">Enviar</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- CITY EVENT2 WRAP END-->
			
			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap requst02">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->
			<?php include 'footer.php'; ?>
		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.nice-select.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
		<!--Map-->
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
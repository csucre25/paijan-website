<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tu Municipalidad - - Municipalidad de Paiján</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 

			<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Paiján</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Home</a></li>
						  <li class="breadcrumb-item"><a href="paijan.php">Paiján</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			<div class="row" style="padding:40px;"></div>
			<!-- CITY SERVICES2 WRAP START-->
			<!--<div class="city_health_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="city_health_text">
								<h2><span>Health and Social</span> Welfare</h2>
								<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh </p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="city_health_fig">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/health-fig.jpg" alt="">
								</figure>
							</div>
						</div>
					</div>
				</div>	
			</div>-->
			<!-- CITY SERVICES2 WRAP END-->
			
			
			<!-- CITY SERVICES2 WRAP START-->
			<div class="city_services2_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-classroom"></i></span>
										<div class="city_service2_caption">
											<h5>HISTORIA</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Escudo</a></li>
										<li><a href="#"><i class="fa fa-star-o"></i>Himno</a></li>
										<li><a href="#"><i class="fa fa-star-o"></i>Población</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig1.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-bag"></i></span>
										<div class="city_service2_caption">
											<h5>UBICACIÓN</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Geografía y clima</a></li>
										<li><a href="#"><i class="fa fa-star-o"></i>Mapa</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig2.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-home"></i></span>
										<div class="city_service2_caption">
											<h5>CIUDAD SEGURA</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Comisarías de Paiján</a></li>
										<li><a href="#"><i class="fa fa-star-o"></i>Télefonos de emergencia</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig2.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-home"></i></span>
										<div class="city_service2_caption">
											<h5>COMITÉ DISTRITAL DE SEGURIDAD CIUDADANA</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>CDSC 2021</a></li>
										<li><a href="#"><i class="fa fa-star-o"></i>CDSC 2020</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>				
					</div>
				</div>
			</div>
			<!-- CITY SERVICES2 WRAP END-->			
			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->
			
			<?php include 'footer.php'; ?>

		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
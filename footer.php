<footer>
				<div class="widget_wrap overlay">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-6">
								<div class="widget_list">
									<h4 class="widget_title">Dirección</h4>
									<div class="widget_text">
										<ul>
											<li><a href="#">Calle Grau #207</a></li>
											<li><a href="#">Trujillo - La Libertad</a></li>
											<li><a href="#">Perú.</a></li>
										</ul>
										<p>Abierto de Lunes a Viernes</p>
										<p>De 8:00 am - 3:00 pm</p>
										<p>Tel. 949900990</p>
									</div> 
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-4 col-sm-6">
										<div class="widget_list">
											<h4 class="widget_title">Explore</h4>
											 <div class="widget_service">
												<ul>
													<li><a href="#">Online Services</a></li>
													<li><a href="#">Residents</a></li>
													<li><a href="#">Business</a></li>
													<li><a href="#">Government</a></li>
													<li><a href="#">Visiting</a></li>
													<li><a href="#">Employment</a></li>
												</ul>
											 </div>
										</div>
									</div>
									<div class="col-md-4 col-sm-6">
										<div class="widget_list">
											<h4 class="widget_title">Features</h4>
											 <div class="widget_service">
												<ul>
													<li><a href="#">Online Services</a></li>
													<li><a href="#">Residents</a></li>
													<li><a href="#">Business</a></li>
													<li><a href="#">Government</a></li>
													<li><a href="#">Visiting</a></li>
													<li><a href="#">Employment</a></li>
												</ul>
											 </div>
										</div>
									</div>
									<div class="col-md-4 col-sm-6">
										<div class="widget_list">
											<h4 class="widget_title">Servicios</h4>
											 <div class="widget_service">
												<ul>
													<li><a href="#">Online Services</a></li>
													<li><a href="#">Residents</a></li>
													<li><a href="#">Business</a></li>
													<li><a href="#">Government</a></li>
													<li><a href="#">Visiting</a></li>
													<li><a href="#">Employment</a></li>
												</ul>
											 </div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="widget_list">
									<h4 class="widget_title">Nuestros horarios</h4>
									<div class="widget_text text2">
										<ul>
											<li><a href="#">Lunes<span>08 am - 3 pm</span></a></li>
											<li><a href="#">Martes<span>08 am - 3 pm</span></a></li>
											<li><a href="#">Miércoles<span>08 am - 3 pm</span></a></li>
											<li><a href="#">Jueves<span>08 am - 3 pm</span></a></li>
											<li><a href="#">Viernes<span>08 am - 3 pm</span></a></li>
											<li><a href="#">Sábado - Domingo<span>Cerrado</span></a></li>
										</ul>
									</div> 
								</div>
							</div>
							<div class="widget_copyright">
								<div class="col-md-3 col-sm-6">
									<div class="widget_logo">
										<a href="#"><img src="images/widget-logo-1.png" alt=""></a>
									</div>
								</div>
								<div class="col-md-6">
									<div class="copyright_text">
										<p><span>Copyright © 2021 - 22 Municipalidad de Paiján.</span> Diseño y desarrollo - Área de TI</p>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="city_top_social">
										<ul>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
											<li><a href="#"><i class="fa fa-google"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>		
				</div>
			</footer>
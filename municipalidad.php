<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tu Municipalidad - Municipalidad de Paiján</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 

			<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Tu Municipalidad</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Home</a></li>
						  <li class="breadcrumb-item"><a href="municipalidad.php">Tu Municipalidad</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			<div class="row" style="padding:40px;"></div>
			<!-- CITY SERVICES2 WRAP START-->
			<!--<div class="city_health_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="city_health_text">
								<h2><span>Health and Social</span> Welfare</h2>
								<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh </p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="city_health_fig">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/health-fig.jpg" alt="">
								</figure>
							</div>
						</div>
					</div>
				</div>	
			</div>-->
			<!-- CITY SERVICES2 WRAP END-->
			
			
			<!-- CITY SERVICES2 WRAP START-->
			<div class="city_service_detail_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<div class="sidebar_widget">
								<!-- CITY SERVICE TABS START-->
								<div class="city_service_tabs tabs">
									<ul class="tab-links">
										<li class="active"><a href="#tab1"> QUIENES SOMOS</a></li>
										<li><a href="#tab2">CONSEJO MUNICIPAL</a></li>
										<li><a href="#tab3">MISIÓN Y VISIÓN</a></li>
										<li><a href="#tab4">LOCALES MUNICIPALES</a></li>
										<li><a href="#tab5">FUNCIONARIOS DE DIRECCIÓN Y CONFIANZA</a></li>
										<li><a href="#tab6">ORGANIGRAMA</a></li>
										<li><a href="#tab7">DIRECTORIO TELEFÓNICO</a></li>
									</ul>
								</div>
								<!-- CITY SERVICE TABS END-->
								
								<!-- CITY SIDE INFO START-->
								<div class="city_side_info">
									<span><i class="fa fa-question-circle"></i></span>
									<h4>Información de Contáctos</h4>
									<h6>908-879-5100 89, <br>Calle Grau #207 <br> Paijan</h6>
								</div>
								<!-- CITY SIDE INFO END-->
								
								<!-- CITY NOTICE START-->
								<div class="city_notice">
									<h4>Public Notice</h4>
									<p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit</p>
									<a class="theam_btn" href="#" tabindex="0">Download PDF</a>
								</div>
								<!-- CITY NOTICE END-->
							</div>
						</div>
						<div class="col-md-9">
							<div class="tabs">
								<div class="tab-content">
									<div id="tab1" class="tab active">
										<div class="city_service_tabs_list">
											<figure class="box">
												<div class="box-layer layer-1"></div>
												<div class="box-layer layer-2"></div>
												<div class="box-layer layer-3"></div>
												<img src="extra-images/service-tabs3.png" alt="">
											</figure>
											<div class="city_service_tabs_text">
												<h3>QUIENES SOMOS</h3>
												<p>Paiján te da la bienvenida a través de este espacio digital, un canal de comunicación de carácter abierto en el que cada día podrás encontrar información del trabajo que venimos realizando para hacer de nuestro Distrito un lugar más seguro, ordenado, limpio, inclusivo y participativo.</p>
												<p>Un Paiján con la que todos soñamos.</p>
												<p>Nuestro Distrito es un Distrito dinámica y atractiva, y nuestro compromiso es repotenciar esas características, pero sobre todo que se convierta en un Paiján mucho más acogedora para ti y para los visitantes que recibimos a diario.</p>
												<p>Estamos trabajando con mucha ilusión, responsabilidad y con absoluta transparencia, esta última será el motor de la presente gestión y que nos impulsará a ofrecerte más y mejores servicios, para optimizar tu calidad de vida y la de tu familia, y todos podamos así disfrutar plenamente de nuestro querido Paijan. ¡Gracias por visitarnos!</p>
											</div>
										</div>
									</div>
									<div id="tab2" class="tab">
										<!--CITY HEALTH TEXT START-->
										<div class="city_health2_text text2">
										<!--SECTION HEADING START-->
											<div class="city_service_tabs_text">
												<h3>Regidores</h3>
											</div>
											<!--SECTION HEADING END-->
											<div class="row">
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-woman.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Regidora</span>
															<h5>Corpus Delicia Tello Cotrina</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">25576930</a>
														</div>
													</div>
												</div>	
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Regidor</span>
															<h5>Elvis Hual Tibamba Rumay</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">80554369</a>
														</div>
													</div>
												</div>	
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-woman.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Regidora</span>
															<h5>Alexandra Jacqueline Lozada Vaso</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">44015433 </a>
														</div>
													</div>
												</div>	
											</div>
											<div class="row">
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Regidor</span>
															<h5>Cesar Eduardo Vigo Gallardo</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">70655831 </a>
														</div>
													</div>
												</div>	
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-woman.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Regidora</span>
															<h5>Beatriz Fabiola Mostacero Urbano</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">18084392</a>
														</div>
													</div>
												</div>	
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Regidor</span>
															<h5>Roger Ruben Capristan  Castillo</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">18906526</a>
														</div>
													</div>
												</div>	
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Regidor</span>
															<h5>Julio Luis Augusto Gil Caballero</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">43470461</a>
														</div>
													</div>
												</div>	
											</div>		
										</div>
										<!--CITY HEALTH TEXT END-->
									</div>
									<div id="tab3" class="tab">
										<div class="city_service_tabs_list">
											<!--<figure class="box">
												<div class="box-layer layer-1"></div>
												<div class="box-layer layer-2"></div>
												<div class="box-layer layer-3"></div>
												<img src="extra-images/service-tabs.jpg" alt="">
											</figure>-->
											<div class="city_service_tabs_text">
												<h3>NUESTRA MISIÓN</h3>
												<p>La Municipalidad Distrital de Paijan es una entidad pública que ejerce competencias y funciones de carácter local y de gobierno Distrital; su gestión está orientada a la prestación de servicios públicos con procesos simplificados y de calidad, promoción del desarrollo económico, ejecución de proyectos de inversión que permitan acortar la brecha de infraestructura de la ciudad otorgando mayor competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
											<div class="city_service_tabs_text">
												<h3>NUESTRA VISIÓN</h3>
												<p>Ser una institución líder en el desarrollo de una ciudad humana, solidaria, moderna, segura y sostenible con el medio ambiente, con una población orgullosa e identificada con la ciudad de Paijan; comprometida con mejorar la calidad de vida de los ciudadanos que menos tienen.</p>
											</div>
										</div>
									</div>
									<div id="tab4" class="tab">
										<div class="">
											<div class="row">
												<div class="col-md-4 col-sm-6">
													<div class="city_health_service2">
														<div class="city_health_list">
															<span class="overlay"><i class="fa fa-building-o"></i></span>
															<h5><span>Anexo I</span></h5>
															<p>Jr. Baquíjano y Carrillo 1198 Sector Central<br> <br> <br></p>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-6">
													<div class="city_health_service2">
														<div class="city_health_list">
															<span class="overlay"><i class="fa fa-building-o"></i></span>
															<h5><span>Anexo II</span></h5>
															<p>Jr. Benito Juárez 1192 Sector Central <br> <br> <br></p>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-6">
													<div class="city_health_service2">
														<div class="city_health_list">
															<span class="overlay"><i class="fa fa-building-o"></i></span>
															<h5><span>Observatorio</span></h5>
															<p>	Esq. Av. José Gabriel Condorcanqui y Calle Los Álamos Mz. A1- Lt. 50 – Manuel Arévalo II Etapa</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="tab5" class="tab">
										<!--CITY HEALTH TEXT START-->
										<div class="city_health2_text text2">
										<!--SECTION HEADING START-->
											<div class="city_service_tabs_text">
												<h3>Funcionarios de dirección y confianza</h3>
											</div>
											<br>
											<!--SECTION HEADING END-->
											<div class="row">
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Alcalde</span>
															<h5>Oswaldo Daniel Alva Iglesias</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">42012554</a>
														</div>
													</div>
												</div>	
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Gerente Municipal</span>
															<h5>Sixto Ramirez Maquina</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">18172283</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Subgerente de obras publicas</span>
															<h5>Juan Julio Roncal Mendoza</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">41934710</a>
														</div>
													</div>
												</div>	
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-woman.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Subgerente de servicios sociales</span>
															<h5>Lili Virginia Ascoy Fernandez</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">18862404</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Subgerente de desarrollo economico y promocion turistica</span>
															<h5>Victor Antonio Guzman Mego</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">70569607</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Subgerente de servicios publicos y gestion ambiental</span>
															<h5>Edgar Jesus Caceda Guerra</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">46018992</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Subgerente de seguridad ciudadana</span>
															<h5>Andres Lino Lopez Perez</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">18141881</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Subgerente de desarrollo urbano y rural</span>
															<h5>Jesus Joel Caceda Epifania</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">40852041</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe de la oficina de Administracion Tributaria</span>
															<h5>Alberto Eduardo Rodriguez Reyna</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">44683175</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe de la oficina de fiscalizacion y control municipal</span>
															<h5>Victor Jhonny Carril Goicochea</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">18115281</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe de la oficina de ejecución coactiva</span>
															<h5>Juan Jose Quispe Alva</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">17919216</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe de la oficina de asesoria juridica</span>
															<h5>Victor Juan Caceda Guerra</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">45272511</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-woman.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe de la oficina de planeamiento y presupuesto</span>
															<h5>Sandra Vasquez Romero</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">19320786</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-woman.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Secretaria General</span>
															<h5>Elkie Milagros Abanto Alayo</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">76513674</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-woman.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Procuradora Municipal</span>
															<h5>Lourdes Sugheyt Valderrama Rojas</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">44401916</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe de la unidad de abastecimiento</span>
															<h5>Raul Humberto Nakasone Vera</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">45983492</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe en la unidad de contabilidad</span>
															<h5>Jose Santos Quezada Tiznado</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">42126624</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-woman.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe de la unidad de tesoreria</span>
															<h5>Lourdes del Pilar Cruzado Leiva</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">42922923</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe de la unidad de gestión de recursos humanos</span>
															<h5>Carlos Augusto Gallo Gonzales</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">04643342</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-woman.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe de control patrimonial</span>
															<h5>Flor Maria Quiroz Quiliche</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">18906522</a>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4">
													<div class="city_senior_team" style="max-width: 100%;">
														<figure class="box">
															<div class="box-layer layer-1"></div>
															<div class="box-layer layer-2"></div>
															<div class="box-layer layer-3"></div>
															<img src="extra-images/business-man-1.png" alt="">
														</figure>
														<div class="city_senior_team_text">
															<span>Jefe del área de tecnologias y sistemas informáticos</span>
															<h5>Jorge Carlos Ruiz Nacarino</h5>
															<!--<a href="#">r.renouf@gov.je</a>-->
															<a href="#">72537517</a>
														</div>
													</div>
												</div>
											</div>		
										</div>
									</div>
									<div id="tab6" class="tab">
										<div class="city_health2_text text2">
											<div class="city_contact_map">
												<iframe src="files/services/organigrama.pdf#view=FitH" frameborder="0" class="ap" height="500"></iframe>
											</div>
										</div>
									</div>
									<div id="tab7" class="tab">
										<div class="city_health2_text text2">
											<div class="row">
												<div class="col-md-4 col-sm-6">
													<div class="city_health_service2">
														<div class="city_health_list">
															<span class="overlay"><i class="fa fa-building-o"></i></span>
															<h5><span>SECRETARÍA DE </span>ALCALDÍA</h5>
															<p>(TELEFONO CENTRAL: 044 - 272478) ANEXO 101</p>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-6">
													<div class="city_health_service2">
														<div class="city_health_list">
															<span class="overlay"><i class="fa fa-building-o"></i></span>
															<h5><span>ALCALDÍA</span></br></h5>
															<p>(TELEFONO CENTRAL: 044 - 272478) ANEXO 102</p>
														</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-6">
													<div class="city_health_service2">
														<div class="city_health_list">
															<span class="overlay"><i class="fa fa-building-o"></i></span>
															<h5><span>SECRETARÍA </span>GENERAL</h5>
															<p>(TELEFONO CENTRAL: 044 - 272478) ANEXO 103</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- CITY DEPARTMENT LIST START-->
							<!--<div class="city_department_list">
								<ul>
									<li>
										<div class="city_department2_fig">
											<figure class="box">
												<div class="box-layer layer-1"></div>
												<div class="box-layer layer-2"></div>
												<div class="box-layer layer-3"></div>
												<img src="extra-images/department-01.jpg" alt="">
											</figure>
											<div class="city_department2_text">
												<h5>Fire Department</h5>
												<p>Poin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio...</p>
											</div>
										</div>
									</li>
									<li>
										<div class="city_department2_fig">
											<div class="city_department2_text text2">
												<h5>Police Department</h5>
												<p>Poin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio....</p>
											</div>
											<figure class="box">
												<div class="box-layer layer-1"></div>
												<div class="box-layer layer-2"></div>
												<div class="box-layer layer-3"></div>
												<img src="extra-images/department-02.jpg" alt="">
											</figure>
										</div>
									</li>
									<li>
										<div class="city_department2_fig">
											<figure class="box">
												<div class="box-layer layer-1"></div>
												<div class="box-layer layer-2"></div>
												<div class="box-layer layer-3"></div>
												<img src="extra-images/department-03.jpg" alt="">
											</figure>
											<div class="city_department2_text">
												<h5>Emergency Disaster Department</h5>
												<p>Poin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio...</p>
											</div>
										</div>
									</li>
								</ul>
							</div>-->
							<!-- CITY DEPARTMENT LIST END-->
							
							<!-- CITY EMERGENCY CALL START-->
							<div class="city_emergency_info">
								<div class="city_emergency_call">
									<h5>Important Telephone Numbers Energency</h5>
									<ul>
										<li><a href="#">Emergency</a></li>
										<li><a href="#">911</a></li>
										<li><a href="#">Fire Department</a></li>
										<li><a href="#">(949) 111 222 </a></li>
										<li><a href="#">Police Department</a></li>
										<li><a href="#">(949) 111 222 </a></li>
										<li><a href="#">Environmental Emergency</a></li>
										<li><a href="#">(949) 111 222 </a></li>
										<li><a href="#">Health Science Centre</a></li>
										<li><a href="#">(949) 111 222 </a></li>
										<li><a href="#">Children's Health and Rehabilitation Centre</a></li>
										<li><a href="#">(949) 111 222 </a></li>
									</ul>
								</div>
							</div>
							<!-- CITY EMERGENCY CALL END-->
						</div>
					</div>	
				</div>		
			</div>			
			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->
			
			<?php include 'footer.php'; ?>

		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
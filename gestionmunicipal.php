<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tu Municipalidad - - Municipalidad de Paiján</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 

			<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Gestión municipal</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Home</a></li>
						  <li class="breadcrumb-item"><a href="gestionmunicipal.php">Gestión municipal</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			<div class="row" style="padding:40px;"></div>
			<!-- CITY SERVICES2 WRAP START-->
			<!--<div class="city_health_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="city_health_text">
								<h2><span>Health and Social</span> Welfare</h2>
								<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh </p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="city_health_fig">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/health-fig.jpg" alt="">
								</figure>
							</div>
						</div>
					</div>
				</div>	
			</div>-->
			<!-- CITY SERVICES2 WRAP END-->
			
			
			<!-- CITY SERVICES2 WRAP START-->
			<div class="city_services2_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-classroom"></i></span>
										<div class="city_service2_caption">
											<h5>PARTICIPACIÓN CIUDADANA</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Participación ciudadana</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig1.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-bag"></i></span>
										<div class="city_service2_caption">
											<h5>DEFENSA CIVIL</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Defensa civil en Paiján</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig2.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-home"></i></span>
										<div class="city_service2_caption">
											<h5>ÓRGANO DE CONTROL INTERNO</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Recomendaciones de auditoría</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig3.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-gear"></i></span>
										<div class="city_service2_caption">
											<h5>CONTROL PATRIMONIAL</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Saneamiento de bienes</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig4.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-bid"></i></span>
										<div class="city_service2_caption">
											<h5>DESARROLLO ECONÓMICO LOCAL</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Licencia de funcionamiento</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig5.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-classroom"></i></span>
										<div class="city_service2_caption">
											<h5>SISTEMA DE CONTROL INTERNO</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Control interno</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig5.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-classroom"></i></span>
										<div class="city_service2_caption">
											<h5>RECONSTRUCCIÓN CON CAMBIOS</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Reconstrucción con cambios 2019</a></li>
										<li><a href="#"><i class="fa fa-star-o"></i>Reconstrucción con cambios 2020</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig5.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-classroom"></i></span>
										<div class="city_service2_caption">
											<h5>TRANSFERENCIA DE GESTIÓN</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Transferencia de la gestión</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig5.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-classroom"></i></span>
										<div class="city_service2_caption">
											<h5>PROGRAMACIÓN MULTIANUAL DE INVERSIONES (PMI)</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Programación multianual de inversiones (PMI)</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig5.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-classroom"></i></span>
										<div class="city_service2_caption">
											<h5>CANASTA BÁSICA FAMILIAR</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Relación de beneficiarios</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig5.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-classroom"></i></span>
										<div class="city_service2_caption">
											<h5>GERENCIA AMBIENTAL</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Plan de trabajo 2021(PECA)</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="city_business_fig">
								<figure class="overlay">
									<img src="extra-images/business_fig5.jpg" alt="">
									<div class="city_service2_list">
										<span><i class="fa icon-classroom"></i></span>
										<div class="city_service2_caption">
											<h5>VACUNACIÓN CONTA LA COVID-19</h5>
										</div>
									</div>
								</figure>
								<div class="city_business_list">
									<ul class="city_busine_detail">
										<li><a href="#"><i class="fa fa-star-o"></i>Vacunación</a></li>
									</ul>
									<a class="see_more_btn" href="#">Ver más... <i class="fa icon-next-1"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- CITY SERVICES2 WRAP END-->	
			
			<?php include 'footer.php'; ?>

		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
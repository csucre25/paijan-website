<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Municipalidad de Paiján - Home</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v5.0"></script>
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 
			
			<?php include 'top.php'; ?>
			
			<!--CITY MAIN BANNER START-->
			<div class="city_main_banner">
				<div class="main-banner-slider">
					<div>
						<figure class="overlay">
							<img src="images/obras/alcalde.jpeg" alt="">
							<div class="banner_text">
								<div class="small_text animated">Municipalidad</div>
								<div class="medium_text animated">Distrital de</div>
								<div class="large_text animated"><span id='text'></span><div class='console-underscore' id='console'>&#95;</div></div>
								<div class="banner_btn">
									<a class="theam_btn animated" href="servicios.php">Servicios</a>
									<a class="theam_btn animated color_celeste" href="transparencia.php">Portal de transparencia</a>
								</div>
								<div class="banner_search_form">
									<label>Busca Aquí</label>
									<div class="banner_search_field animated">
										<input type="text" placeholder="Que deseas hacer">
										<a href="#"><i class="fa fa-search"></i></a>
									</div>
								</div>
							</div>
						</figure>
					</div>
					<div>
						<figure class="overlay">
							<img src="images/obras/fotoportada.jpeg" alt="">
							<div class="banner_text">
								<div class="small_text animated">Municipalidad</div>
								<div class="medium_text animated">Distrital de</div>
								<div class="large_text animated">Paiján</div>
								<div class="banner_btn">
									<a class="theam_btn animated" href="servicios.php">Servicios</a>
									<a class="theam_btn animated color_celeste" href="transparencia.php">Portal de transparencia</a>
								</div>
								<div class="banner_search_form">
									<label>Busca Aquí</label>
									<div class="banner_search_field animated">
										<input type="text" placeholder="Que deseas hacer">
										<a href="#"><i class="fa fa-search"></i></a>
									</div>
								</div>
							</div>
						</figure>
					</div>
					<div>
						<figure class="overlay">
							<img src="extra-images/main-banner1-1.jpg" alt="">
							<div class="banner_text">
								<div class="small_text animated">Municipalidad</div>
								<div class="medium_text animated">Distrital de</div>
								<div class="large_text animated">Paiján</div>
								<div class="banner_btn">
									<a class="theam_btn animated" href="servicios.php">Servicios</a>
									<a class="theam_btn animated color_celeste" href="transparencia.php">Portal de transparencia</a>
								</div>
								<div class="banner_search_form">
									<label>Busca Aquí</label>
									<div class="banner_search_field animated">
										<input type="text" placeholder="Que deseas hacer">
										<a href="#"><i class="fa fa-search"></i></a>
									</div>
								</div>
							</div>
						</figure>
					</div>
				</div>
			</div>
			<!--CITY MAIN BANNER END-->
			
			<!--CITY BANNER SERVICES START-->
			<div class="city_banner_services">
				<div class="container-fluid">
					<div class="city_service_list">
						<ul>
							<li>
								<div class="city_service_text">
									<span><i class="fa icon-cursor"></i></span>
									<h5><a href="#enlaces_popular">Enlaces más buscados </a></h5>
								</div>
							</li>
							<li>
								<div class="city_service_text">
									<span><i class="fa icon-law"></i></span>
									<h5><a href="#enlaces_documentos">Enlaces documentos </a></h5>
								</div>
							</li>
							<li>								
							    <div class="city_service_text">
									<span><i class="fa icon-news"></i></span>
									<h5><a href="#noticias_publi">Noticias y publicaciones</a></h5>
								</div>
							</li>
							<!--<li>
								<div class="city_service_text">
									<span><i class="fa icon-trash"></i></span>
									<h5><a href="#">Vive Paiján</a></h5>
								</div>
							</li>	-->						
						</ul>
					</div>
				</div>
			</div>
			<!--CITY BANNER SERVICES END-->
			
			<!--CITY ABOUT WRAP START-->
			<div class="city_about_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="city_about_fig">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="images/obras/fotoalcalde.jpeg" alt="">
								</figure>
								<!--<div class="city_about_video">-->
								
								
									<!--<video id="mivideo" width=400px  controls  preload="auto" autoplay poster="videos/INICIO.jpg">
	 									<source src="videos/presentacion.wmv" type="video/wmv" >
										<source src="videos/presentacion.mp4"  type="video/mp4">
										<source src="videos/presentacion.avi" type="video/avi">
								    </video>
											
									<script>
									mivideo = document.getElementById('mivideo');
									mivideo.volume = 2;
									mivideo.autoplay = true;
									mivideo.loop = true;
									mivideo.load();
									</script>-->
						 	
					 				
									<!--<figure class="overlay">
										<img src="extra-images/about_video.jpg" alt="">
										<a class="paly_btn hvr-pulse" data-rel="prettyPhoto" href="https://www.youtube.com/watch?v=SAaevusBnNI"><i class="fa icon-play-button"></i></a>
									</figure>
								</div>-->
							</div>
						</div>
						<div class="col-md-6">
							<div class="city_about_list">
								<!--SECTION HEADING START-->
								<div class="section_heading border">
									<span>Bienvenido al sitio web</span>
									<h2>Municipalidad Paiján</h2>
								</div>
								<!--SECTION HEADING END-->
								<div class="city_about_text">
									<h6>Sitio web oficial 2021.</h6>
									<p>Paiján te da la bienvenida a través de este espacio digital, un canal de comunicación de carácter abierto en el que cada día podrás encontrar información del trabajo que venimos realizando para hacer de nuestro distrito un lugar más seguro, ordenado, limpio, inclusivo y participativo.</p>
								</div>
								<ul class="city_about_link">
									<li><a href="municipalidad.php"><i class="fa fa-star"></i>Tu municipalidad</a></li>
									<li><a href="gestionmunicipal.php"><i class="fa fa-star"></i>Gestión Municipal</a></li>
									<li><a href="servicios.php"><i class="fa fa-star"></i>Servicios Online</a></li>
									<li><a href="tupa.php"><i class="fa fa-star"></i>Trámites</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--CITY ABOUT WRAP END-->

			<!--CITY ABOUT WRAP START-->
			<div id="enlaces_popular" class="city_department_wrap goverment">
				<div class="container">
					<!--SECTION HEADING START-->
					<div class="section_heading margin-bottom">
						<span>Explorar</span>
						<h2>Enlaces más buscados</h2>
					</div>
					<!--SECTION HEADING END-->
					<div class="row">
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">
									<a  href="tupa.php"><img src="extra-images/department-fig-1.jpg" alt=""></a>								
									</figure>
									<div class="city_department_text">
										<a  href="tupa.php"><h5>TUPA </h5></a>
										<p href="tupa.php">Texto Único de Procedimiento Administrativos </p>
										<a href="tupa.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>	
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">									
									<a  href="mesapartes.php"><img src="extra-images/department-fig1-1.jpg" alt=""></a>
									</figure>
									<div class="city_department_text">
									    <a  href="mesapartes.php"><h5>MESA DE PARTES </h5></a>
										<p>Virtual </p>
										<a href="mesapartes.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">
										<img src="extra-images/department-fig2-1.jpg" alt="">
									</figure>
									<div class="city_department_text">
										<h5>CALL CENTER</h5>
										<p>Centro de atención al ciudadano </p>
										<a href="#">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">
										<img src="extra-images/department-fig3-1.png" alt="">
									</figure>
									<div class="city_department_text">
										<h5>INSTRUMENTOS DE GESTIÓN</h5>
										<p>Información del ROF, MOD, CAP, TUPA, Planes y Politicas </p>
										<a href="instrumentos.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="width_control">								
								<div class="city_department_fig">
									<figure class="">
										<img src="extra-images/department-fig4-1.png" alt="">
									</figure>
									<div class="city_department_text">
										<h5>LICENCIA DE FUNCIONAMIENTO</h5>
										<p>La manera más sencilla de obtener una licencia </p>
										<a href="#">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">
										<img src="extra-images/department-fig5-1.jpg" alt="">
									</figure>
									<div class="city_department_text">
										<h5>CONVOCATORIA CAS</h5>
										<p>Contratación Administrativa de Servicios - D.L. Nº 1057 </p>
										<a href="bolsa.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">
									<a href="tributos.php"><img src="extra-images/department-fig6-1.jpg" alt=""></a>
									</figure>
									<div class="city_department_text">
									<a href="tributos.php"><h5>TRIBUTOS MUNICIPALES</h5></a>
										<p>Información sobre tributos municipales </p>
										<a href="tributos.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>	
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">
									<a href="licenciaedificacion.php"><img src="extra-images/department-fig7-1.jpg" alt=""></a>
									</figure>
									<div class="city_department_text">
									<a href="licenciaedificacion.php"><h5>LICENCIA DE EDIFICACIÓN </h5></a>
										<p>Formularios para la obtención de licencia </p>
										<a href="licenciaedificacion.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="width_control">								
								<div class="city_department_fig">
									<figure class="">
										<img src="extra-images/department-fig8-1.jpg" alt="">
									</figure>
									<div class="city_department_text">
										<h5>SERVICIOS EN LÍNEA</h5>
										<p>Accede a nuestros servicios online </p>
										<a href="servicios.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>	
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">
										<a href="libroreclamacion.php"><img src="extra-images/department-fig9-1.jpg" alt=""></a>
									</figure>
									<div class="city_department_text">
									<a href="libroreclamacion.php"><h5>LIBRO DE RECLAMACIONES</h5></a>
										<p>Decreto supremo Nº 042-2011-PCM </p>
										<a href="libroreclamacion.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">
										<a href="vacunacion.php"><img src="extra-images/vacuna.jpg" alt=""></a>
									</figure>
									<div class="city_department_text">
										<a href="vacunacion.php"><h5>VACUNACIÓN</h5></a>
										<p>Vacunas </p>
										<a href="vacunacion.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">
										<a href="obras.php"><img src="extra-images/obra.jpg" alt=""></a>
									</figure>
									<div class="city_department_text">
										<a href="obras.php"><h5>OBRAS</h5></a>
										<p>Obras </p>
										<a href="obras.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="">
									<a href="registrocivil.php"><img src="extra-images/regitrocivil.png" alt=""></a>
									</figure>
									<div class="city_department_text">
									<a href="registrocivil.php"><h5>REGISTRO CIVIL </h5></a>
										<p>Requisitos para matrimonio civil </p>
										<a href="registrocivil.php">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
						<!--<div class="col-md-4 col-sm-6">
							<div class="width_control">
								<div class="city_department_fig">
									<figure class="box">
										<div class="box-layer layer-1"></div>
										<div class="box-layer layer-2"></div>
										<div class="box-layer layer-3"></div>
										<img src="extra-images/department-fig5.jpg" alt="">
										<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
									</figure>
									<div class="city_department_text">
										<h5>Travel & Tourism</h5>
										<p>This is Photoshop's version  of Ipsum. </p>
										<a href="#">Acceder<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>-->
						<!--<div class="col-md-12">
							<div class="city_service2_btn btn2">
								<a class="theam_btn border-color color" href="#" tabindex="0">See More</a>
							</div>
						</div>-->
					</div>
				</div>
			</div>	
			<!--CITY ABOUT WRAP START-->
			
			<!--CITY DEPARTMENT WRAP START-->
			<div id="enlaces_documentos" class="city_department_wrap overlay">
				<div class="bg_white">
					<div class="container-fluid">
						<!--SECTION HEADING START-->
						<div class="section_heading margin-bottom">
							<span>Explorar</span>
							<h2>Enlaces de documentos</h2>
						</div>
						<!--SECTION HEADING END-->
						<div class="city-department-slider">
							<div>
								<div class="width_control">
									<div class="city_department_fig">
										<figure class="">
											<img src="extra-images/department-fig11-1.jpg" alt="">
										</figure>
										<div class="city_department_text">
											<h5>ORDENANZAS MUNICIPALES</h5>
											<p>Documentos </p>
											<a href="#">Acceder<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
								<div class="width_control">
									<div class="city_department_fig">
										<figure class="">
											<img src="extra-images/department-fig11-1.jpg" alt="">
										</figure>
										<div class="city_department_text">
											<h5>RESOLUCIONES DE ALCALDÍA </h5>
											<p>Documentos </p>
											<a href="#">Acceder<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
								<div class="width_control">
									<div class="city_department_fig">
										<figure class="">
											<img src="extra-images/department-fig11-1.jpg" alt="">
										</figure>
										<div class="city_department_text">
											<h5>DECRETOS DE ALCALDÍA</h5>
											<p>Documentos </p>
											<a href="#">Acceder<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
								<div class="width_control">
									<div class="city_department_fig">
										<figure class="">
											<img src="extra-images/department-fig11-1.jpg" alt="">
										</figure>
										<div class="city_department_text">
											<h5>ACUERDOS DE CONSEJO</h5>
											<p>Documentos </p>
											<a href="#">Acceder<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
								<!--<div class="width_control">								
									<div class="city_department_fig">
										<figure class="box">
											<div class="box-layer layer-1"></div>
											<div class="box-layer layer-2"></div>
											<div class="box-layer layer-3"></div>
											<img src="extra-images/department-fig4.jpg" alt="">
											<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
										</figure>
										<div class="city_department_text">
											<h5>Travel & Tourism</h5>
											<p>This is Photoshop's version  of Ipsum. </p>
											<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
								<div class="width_control">
									<div class="city_department_fig">
										<figure class="box">
											<div class="box-layer layer-1"></div>
											<div class="box-layer layer-2"></div>
											<div class="box-layer layer-3"></div>
											<img src="extra-images/department-fig5.jpg" alt="">
											<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
										</figure>
										<div class="city_department_text">
											<h5>Police Department</h5>
											<p>This is Photoshop's version  of Ipsum. </p>
											<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
							</div>
							<div>
								<div class="width_control">
									<div class="city_department_fig">
										<figure class="box">
											<div class="box-layer layer-1"></div>
											<div class="box-layer layer-2"></div>
											<div class="box-layer layer-3"></div>
											<img src="extra-images/department-fig.jpg" alt="">
											<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
										</figure>
										<div class="city_department_text">
											<h5>Administration</h5>
											<p>This is Photoshop's version  of Ipsum. </p>
											<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
								<div class="width_control">
									<div class="city_department_fig">
										<figure class="box">
											<div class="box-layer layer-1"></div>
											<div class="box-layer layer-2"></div>
											<div class="box-layer layer-3"></div>
											<img src="extra-images/department-fig1.jpg" alt="">
											<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
										</figure>
										<div class="city_department_text">
											<h5>Employment </h5>
											<p>This is Photoshop's version  of Ipsum. </p>
											<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
								<div class="width_control">								
									<div class="city_department_fig">
										<figure class="box">
											<div class="box-layer layer-1"></div>
											<div class="box-layer layer-2"></div>
											<div class="box-layer layer-3"></div>
											<img src="extra-images/department-fig2.jpg" alt="">
											<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
										</figure>
										<div class="city_department_text">
											<h5>Education</h5>
											<p>This is Photoshop's version  of Ipsum. </p>
											<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
								<div class="width_control">
									<div class="city_department_fig">
										<figure class="box">
											<div class="box-layer layer-1"></div>
											<div class="box-layer layer-2"></div>
											<div class="box-layer layer-3"></div>
											<img src="extra-images/department-fig3.jpg" alt="">
											<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
										</figure>
										<div class="city_department_text">
											<h5>Health Care</h5>
											<p>This is Photoshop's version  of Ipsum. </p>
											<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
								<div class="width_control">
									<div class="city_department_fig">
										<figure class="box">
											<div class="box-layer layer-1"></div>
											<div class="box-layer layer-2"></div>
											<div class="box-layer layer-3"></div>
											<img src="extra-images/department-fig4.jpg" alt="">
											<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
										</figure>
										<div class="city_department_text">
											<h5>Travel & Tourism</h5>
											<p>This is Photoshop's version  of Ipsum. </p>
											<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>
								<div class="width_control">
									<div class="city_department_fig">
										<figure class="box">
											<div class="box-layer layer-1"></div>
											<div class="box-layer layer-2"></div>
											<div class="box-layer layer-3"></div>
											<img src="extra-images/department-fig5.jpg" alt="">
											<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/department-fig.jpg"><i class="fa fa-plus"></i></a>
										</figure>
										<div class="city_department_text">
											<h5>Police Department</h5>
											<p>This is Photoshop's version  of Ipsum. </p>
											<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
								</div>-->
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY DEPARTMENT WRAP END-->
			
			<!--CITY OFFICE WRAP START-->
			<!--<div class="city_office_wrap">
				<div class="bg_white bg_none">
					<div class="container-fluid">
						<div class="city_office_row">
							<div class="city_triagle">
								<span></span>
							</div>
							<div class="center_text">
								<div class="city_office_list">
									<div class="city_office_text">
										<h6>City</h6>
										<h3>Mayor’s Office</h3>
										<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
									</div>
									<div class="city_office_logo">
										<a href="#"><img src="images/office-logo.png" alt=""></a>
									</div>
									<div class="city_office_text pull_right">
										<h6>City</h6>
										<h3>Council Office</h3>
										<a href="#">See More<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
							<div class="city_triagle text-right">
								<span></span>
							</div>
						</div>
					</div>
				</div>
			</div>-->
			<!--CITY OFFICE WRAP END-->		

			<!--CITY BLOG WRAP START-->
			<!--<div id="noticias_publi" class="city_blog_wrap">-->
				<!--<div class="container">-->
					<!--SECTION HEADING START-->
					<!--<div class="heding_full">
						<div class="section_heading">
							<span>Explorar</span>
							<h2>NOTICIAS Y PUBLICACIONES</h2>
						</div>
						<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet Aenean sollicitudin, lorem quis bibendum auctor Proin gravida nibh vel velit auctor aliquet Aenean sollicitudin, lorem quis bibendum auctor</p>
					</div>-->
					<!--SECTION HEADING END-->
					<!--<div class="row">
						<div class="col-md-4 col-sm-4">
							<div class="city_blog_fig">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/blog-fig.jpg" alt="">
								</figure>
								<div class="city_blog_text">
									<span>Community</span>
									<h4>Animal Control</h4>
									<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
									<div class="city_blog_social">
										<a class="theam_btn border-color color" href="#" tabindex="0">Read More</a>
										<div class="city_blog_icon_list">
											<ul class="social_icon">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-google"></i></a></li>
											</ul>
											<a class="share_icon" href="#"><i class="fa icon-social"></i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="city_blog_fig position">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/blog-hover.jpg" alt="">
								</figure>
								<div class="city_blog_text">
									<span>Community</span>
									<h4>Animal Control</h4>
									<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
									<div class="city_blog_social">
										<a class="theam_btn border-color color" href="#" tabindex="0">Read More</a>
										<div class="city_blog_icon_list">
											<ul class="social_icon">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-google"></i></a></li>
											</ul>
											<a class="share_icon" href="#"><i class="fa icon-social"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="city_blog_fig">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/blog-fig1.jpg" alt="">
								</figure>
								<div class="city_blog_text">
									<span>Community</span>
									<h4>Job Seeker</h4>
									<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
									<div class="city_blog_social">
										<a class="theam_btn border-color color" href="#" tabindex="0">Read More</a>
										<div class="city_blog_icon_list">
											<ul class="social_icon">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-google"></i></a></li>
											</ul>
											<a class="share_icon" href="#"><i class="fa icon-social"></i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="city_blog_fig position">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/blog-hover.jpg" alt="">
								</figure>
								<div class="city_blog_text">
									<span>Community</span>
									<h4>Job Seeker</h4>
									<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
									<div class="city_blog_social">
										<a class="theam_btn border-color color" href="#" tabindex="0">Read More</a>
										<div class="city_blog_icon_list">
											<ul class="social_icon">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-google"></i></a></li>
											</ul>
											<a class="share_icon" href="#"><i class="fa icon-social"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="city_blog_fig">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/blog-fig2.jpg" alt="">
								</figure>
								<div class="city_blog_text">
									<span>Community</span>
									<h4>Senior Citizen</h4>
									<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
									<div class="city_blog_social">
										<a class="theam_btn border-color color" href="#" tabindex="0">Read More</a>
										<div class="city_blog_icon_list">
											<ul class="social_icon">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-google"></i></a></li>
											</ul>
											<a class="share_icon" href="#"><i class="fa icon-social"></i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="city_blog_fig position">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/blog-hover.jpg" alt="">
								</figure>
								<div class="city_blog_text">
									<span>Community</span>
									<h4>Senior Citizen</h4>
									<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctornisi elit.</p>
									<div class="city_blog_social">
										<a class="theam_btn border-color color" href="#" tabindex="0">Read More</a>
										<div class="city_blog_icon_list">
											<ul class="social_icon">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-google"></i></a></li>
											</ul>
											<a class="share_icon" href="#"><i class="fa icon-social"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>-->
			<!--CITY BLOG WRAP END-->
			
			<!--CITY JOBS WRAP START-->
			<!--<div class="city_jobs_wrap">
				<div class="city_jobs_fig">
					<div class="city_job_text">
						<span>Mayor of the city</span>
						<h2>Meet Dorien Daniels</h2>
						<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis </p>
						<a class="theam_btn" href="#" tabindex="0">Get In Touch</a>
					</div>
				</div>
				<div class="city_jobs_list">
					<ul>
						<li>
							<div class="city_jobs_item overlay">
								<span><i class="fa icon-team"></i></span>
								<div class="ciy_jobs_caption">
									<h2>Living Here</h2>
									<p>This is Photoshop's ersion  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin</p>
									<a href="#">Find Out More</a>
								</div>
							</div>
						</li>
						<li>
							<div class="city_jobs_item pull-right overlay">
								<div class="ciy_jobs_caption">
									<h2>Visit Here</h2>
									<p>This is Photoshop's ersion  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin</p>
									<a href="#">Find Out More</a>
								</div>
								<span><i class="fa icon-urban"></i></span>
							</div>
						</li>
						<li>
							<div class="city_jobs_item overlay">
								<span><i class="fa icon-portfolio"></i></span>
								<div class="ciy_jobs_caption">
									<h2>Doing Busines Here</h2>
									<p>This is Photoshop's ersion  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin</p>
									<a href="#">Find Out More</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>-->	
			<!--CITY JOBS WRAP END-->
			
			<!--CITY PROJECT WRAP START-->
			<!--<div class="city_project_wrap">
				<div class="container-fluid">-->
					<!--SECTION HEADING START-->
					<!--<div class="section_heading center">
						<span>Goverment</span>
						<h2>Programs & Projects</h2>
					</div>-->
					<!--SECTION HEADING END-->
				<!--	<div class="city_project_mansory">
						<ul id="filterable-item-filter-1">
							<li><a data-value="all" href="#">All</a></li>
							<li><a data-value="1" href="#">Categories</a></li>
							<li><a data-value="2" href="#">Eco</a></li>
							<li><a data-value="3" class="active" href="#">Programs</a></li>
							<li><a data-value="4" href="#">Social life</a></li>
							<li><a data-value="5" href="#">Sport</a></li>
							<li><a data-value="6" href="#">Technology</a></li>
						</ul>
					</div>
					<div class="city-project-slider">
						<div>
							<div class="city_project_fig">
								<figure class="overlay">
									<img src="extra-images/project-fig.jpg" alt="">
									<div class="city_project_text">
										<span><i class="fa icon-plastic"></i></span>
										<a href="#">Constructions</a>
										<h3><a href="#">Government vows to prioritize local contractors in infrastructure program</a></h3>
									</div>
								</figure>
							</div>
						</div>
					
						<div>
							<div class="city_project_fig">
								<figure class="overlay">
									<img src="extra-images/project-fig1.jpg" alt="">
									<div class="city_project_text">
										<span><i class="fa icon-sun"></i></span>
										<a href="#">Goverment Solar Project</a>
										<h3><a href="#">Businesses Should Accelerate Their Renewable Energy Strategy</a> </h3>
									</div>
								</figure>
							</div>
						</div>
						
						<div>
							<div class="city_project_fig">
								<figure class="overlay">
									<img src="extra-images/project-fig2.jpg" alt="">
									<div class="city_project_text">
										<span><i class="fa icon-partner"></i></span>
										<a href="#">Citizen Program</a>
										<h3><a href="#">Government vows to prioritize local contractors in infrastructure program</a></h3>
									</div>
								</figure>
							</div>
						</div>
						<div>
							<div class="city_project_fig">
								<figure class="overlay">
									<img src="extra-images/project-fig2.jpg" alt="">
									<div class="city_project_text">
										<span><i class="fa icon-plastic"></i></span>
										<a href="#">Citizen Program</a>
										<h3><a href="#">Government vows to prioritize local contractors in infrastructure program</a></h3>
									</div>
								</figure>
							</div>
						</div>
					</div>
				</div>
			</div>	-->
			<!--CITY PROJECT WRAP END-->	

			<!--CITY EVENT WRAP START-->
			<!--<div class="city_event_wrap">
				<div class="bg_white width">
					<div class="container-fluid">-->
						<!--SECTION HEADING START-->
						<!--<div class="heding_full">
							<div class="section_heading">
								<span>Upcoming</span>
								<h2>Feature Events & Meeting</h2>
							</div>
							<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet Aenean sollicitudin, lorem quis bibendum auctor Proin gravida nibh vel velit auctor aliquet Aenean sollicitudin, lorem quis bibendum auctor</p>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="city_event_fig">
									<figure class="box">
										<div class="box-layer layer-1"></div>
										<div class="box-layer layer-2"></div>
										<div class="box-layer layer-3"></div>
										<img src="extra-images/event-fig.jpg" alt="">
										<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/event-fig.jpg" tabindex="-1"><i class="fa fa-plus"></i></a>
									</figure>
									<div class="city_event_text">
										<div class="city_event_history">
											<div class="event_date">
												<span>25</span>
												<strong>SEP, 2018</strong>
											</div>
											<div class="city_date_text">
												<h5 class="custom_size">Mayor Conferences</h5>
												<a href="#"><i class="fa fa-clock-o"></i>05:23 AM - 09:23 AM </a>
											</div>
										</div>
										<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean..</p>
										<a href="#">Speaker : <span>Alexzender</span></a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="city_event_fig">
									<figure class="box">
										<div class="box-layer layer-1"></div>
										<div class="box-layer layer-2"></div>
										<div class="box-layer layer-3"></div>
										<img src="extra-images/event-fig1.jpg" alt="">
										<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/event-fig1.jpg" tabindex="-1"><i class="fa fa-plus"></i></a>
									</figure>
									<div class="city_event_text">
										<div class="city_event_history">
											<div class="event_date">
												<span>24</span>
												<strong>SEP, 2018</strong>
											</div>
											<div class="city_date_text">
												<h5 class="custom_size">Culture & Recreation</h5>
												<a href="#"><i class="fa fa-clock-o"></i>05:23 AM - 09:23 AM </a>
											</div>
										</div>
										<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean..</p>
										<a href="#">Speaker : <span>Alexzender</span></a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="city_event_fig">
									<figure class="box">
										<div class="box-layer layer-1"></div>
										<div class="box-layer layer-2"></div>
										<div class="box-layer layer-3"></div>
										<img src="extra-images/event-fig2.jpg" alt="">
										<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/event-fig2.jpg" tabindex="-1"><i class="fa fa-plus"></i></a>
									</figure>
									<div class="city_event_text">
										<div class="city_event_history">
											<div class="event_date">
												<span>20</span>
												<strong>SEP, 2018</strong>
											</div>
											<div class="city_date_text">
												<h5 class="custom_size">Fundraise Event</h5>
												<a href="#"><i class="fa fa-clock-o"></i>05:23 AM - 09:23 AM </a>
											</div>
										</div>
										<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean..</p>
										<a href="#">Speaker : <span>Alexzender</span></a>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="city_event_fig">
									<figure class="box">
										<div class="box-layer layer-1"></div>
										<div class="box-layer layer-2"></div>
										<div class="box-layer layer-3"></div>
										<img src="extra-images/event-fig3.jpg" alt="">
										<a class="paly_btn" data-rel="prettyPhoto" href="extra-images/event-fig3" tabindex="-1"><i class="fa fa-plus"></i></a>
									</figure>
									<div class="city_event_text">
										<div class="city_event_history">
											<div class="event_date">
												<span>15</span>
												<strong>SEP, 2018</strong>
											</div>
											<div class="city_date_text">
												<h5 class="custom_size">Coporate Meetings</h5>
												<a href="#"><i class="fa fa-clock-o"></i>05:23 AM - 09:23 AM </a>
											</div>
										</div>
										<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean..</p>
										<a href="#">Speaker : <span>Alexzender</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>-->
			<!--CITY EVENT WRAP END-->
			
			<!--CITY CLIENT WRAP START-->
			<!--<div class="city_client_wrap">
				<div class="container">
					<div class="city_client_row">
						<ul class="bxslider bx-pager">
							<li>
								<div class="city_client_fig">
									<figure class="box">
										<div class="box-layer layer-1"></div>
										<div class="box-layer layer-2"></div>
										<div class="box-layer layer-3"></div>
										<img src="extra-images/client.jpg" alt="">
									</figure>
									<div class="city_client_text">
										<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis. </p>
										<h4><a href="#">Likor Stom</a> </h4>
										<span><a href="#">Directio-Baseline</a></span>
									</div>
								</div>
							</li>
							<li>
								<div class="city_client_fig">
									<figure class="box">
										<div class="box-layer layer-1"></div>
										<div class="box-layer layer-2"></div>
										<div class="box-layer layer-3"></div>
										<img src="extra-images/client.jpg" alt="">
									</figure>
									<div class="city_client_text">
										<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis. </p>
										<h4><a href="#">Likor Stom</a> </h4>
										<span><a href="#">Directio-Baseline</a></span>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="city_client_link" id="bx-pager">
						<a data-slide-index="0" href="">
							<div class="client_arrow">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
								<img src="extra-images/client1.jpg" alt="">
								</figure>
							</div>	
						</a>
						<a data-slide-index="1" href="">					
							<div class="client_arrow">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/client2.jpg" alt="">
								</figure>
							</div>
						</a>	
					</div>
				</div>
			</div>-->
			<!--CITY CLIENT WRAP END-->
			
			<!--CITY NEWS WRAP START-->
			<!--<div class="city_news_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-8">-->
							<!--SECTION HEADING START-->
							<!--<div class="section_heading margin-bottom">
								<span>Welcome to Local City</span>
								<h2>News Releases</h2>
							</div>-->
							<!--SECTION HEADING START-->
						<!--	<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="city_news_fig">
										<figure class="box">
											<div class="box-layer layer-1"></div>
											<div class="box-layer layer-2"></div>
											<div class="box-layer layer-3"></div>
											<img src="extra-images/news-fig.jpg" alt="">
										</figure>
										<div class="city_news_text">
											<h2>A Fundraiser for the City Club</h2>
											<ul class="city_news_meta">
												<li><a href="#">May 22, 2018</a></li>
												<li><a href="#">Public Notices</a></li>
											</ul>
											<p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sollicitudin</p>
											<a class="theam_btn border-color color" href="#" tabindex="0">Read More</a>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="city_news_row">
										<ul>
											<li>
												<div class="city_news_list">
													<figure class="box">
														<div class="box-layer layer-1"></div>
														<div class="box-layer layer-2"></div>
														<div class="box-layer layer-3"></div>
														<img src="extra-images/news-fig1.jpg" alt="">
													</figure>
													<div class="city_news_list_text">
														<h5>Lorem Ipsum Proin gravida nibh </h5>
														<ul class="city_news_meta">
															<li><a href="#">May 22, 2018</a></li>
															<li><a href="#">Public Notices</a></li>
														</ul>
													</div>
												</div>
											</li>
											<li>
												<div class="city_news_list">
													<figure class="box">
														<div class="box-layer layer-1"></div>
														<div class="box-layer layer-2"></div>
														<div class="box-layer layer-3"></div>
														<img src="extra-images/news-fig2.jpg" alt="">
													</figure>
													<div class="city_news_list_text">
														<h5>Lorem Ipsum Proin gravida nibh </h5>
														<ul class="city_news_meta">
															<li><a href="#">May 22, 2018</a></li>
															<li><a href="#">Public Notices</a></li>
														</ul>
													</div>
												</div>
											</li>
											<li>
												<div class="city_news_list">
													<figure class="box">
														<div class="box-layer layer-1"></div>
														<div class="box-layer layer-2"></div>
														<div class="box-layer layer-3"></div>
														<img src="extra-images/news-fig3.jpg" alt="">
													</figure>
													<div class="city_news_list_text">
														<h5>Lorem Ipsum Proin gravida nibh </h5>
														<ul class="city_news_meta">
															<li><a href="#">May 22, 2018</a></li>
															<li><a href="#">Public Notices</a></li>
														</ul>
													</div>
												</div>
											</li>
											<li>
												<div class="city_news_list">
													<figure class="box">
														<div class="box-layer layer-1"></div>
														<div class="box-layer layer-2"></div>
														<div class="box-layer layer-3"></div>
														<img src="extra-images/news-fig4.jpg" alt="">
													</figure>
													<div class="city_news_list_text">
														<h5>Lorem Ipsum Proin gravida nibh </h5>
														<ul class="city_news_meta">
															<li><a href="#">May 22, 2018</a></li>
															<li><a href="#">Public Notices</a></li>
														</ul>
													</div>
												</div>
											</li>
											<li>
												<div class="city_news_list">
													<figure class="box">
														<div class="box-layer layer-1"></div>
														<div class="box-layer layer-2"></div>
														<div class="box-layer layer-3"></div>
														<img src="extra-images/news-fig5.jpg" alt="">
													</figure>
													<div class="city_news_list_text">
														<h5>Lorem Ipsum Proin gravida nibh </h5>
														<ul class="city_news_meta">
															<li><a href="#">May 22, 2018</a></li>
															<li><a href="#">Public Notices</a></li>
														</ul>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>	
						</div>
						<div class="col-md-4">
							<div class="city_news_form">
								<div class="city_news_feild">
									<span>Signup</span>
									<h4>Newsletter</h4>
									<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida </p>
									<div class="city_news_search">
										<input type="text" name="text" placeholder="Enter Your Email Adress Here" required>
										<button class="theam_btn border-color color">Subcribe Now</button>
									</div>
								</div>
								<div class="city_news_feild feild2">
									<span>Recent</span>
									<h4>Documents</h4>
									<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida </p>
									<div class="city_document_list">
										<ul>
											<li><a href="#"><i class="fa icon-document"></i>Council Agenda April 24, 2015 (27 kB)</a></li>
											<li><a href="#"><i class="fa icon-document"></i>Council Agenda April 24, 2015 (27 kB)</a></li>
											<li><a href="#"><i class="fa icon-document"></i>Council Agenda April 24, 2015 (27 kB)</a></li>
											<li><a href="#"><i class="fa icon-document"></i>Council Agenda April 24, 2015 (27 kB)</a></li>
											<li><a href="#"><i class="fa icon-document"></i>Council Agenda April 24, 2015 (27 kB)</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	-->
			<!--CITY NEWS WRAP END-->

			<!--CITY ABOUT WRAP START-->
			<div id="noticias_publi" class="city_department_wrap goverment">
				<div class="container">
					<!--SECTION HEADING START-->
					<div class="section_heading margin-bottom">
						<span>Explorar</span>
						<h2>NOTICIAS Y PUBLICACIONES</h2>
					</div>
					<!--SECTION HEADING END-->
					<div class="row">
						<div class="col-md-12">
							<div class="width_control">
								<center><div class="fb-page" data-href="https://www.facebook.com/Municipalidad-Distrital-de-Paij%C3%A1n-1962109457238478/?__tn__=kCH-R&amp;eid=ARBdg-UjMFiLtTuL7jErFRDQt11D1xfMm5Oau8-dLfqS9r_Nex9SxqxEYLbxGwIwuXZe6tB12pnl8_sh&amp;hc_ref=ARTMzZ0r8qVgbDWtNzpTVrsd-9eh4V34lldYCJPyVeY8AEDZqfKqU8ctP9052lI8oik&amp;fref=nf&amp;__xts__[0]=68.ARB3nvOsovF47JaQ8ou507VDEgNRjRA7MmZDE50LR1EG-Qw1iYTprggAMgt0a_YI5ahKvktvJpumoMQBZbW1cWJgYO5ycBle2_HRPz65S-wfTwslSJmbjU7VJQWuFzC7t3Q3zOCAnxIWN_655-br_s8tIcrrhvgq2BkemlaIG2tclIoIwymQyG7CK_vFIeVopy4VWGGuKenp5E-HjFfuE35Rz57HM_fDCLnJ0gi2yW4JbsM3vGyfy2UPebuMaRwgOUQRSVV-GdWzoDmiz8bj22Y3FnT9OpAM5J0LIOAxbdtr9JlNKdjAnkaTOo6lfbeYxU06CbYlraHmKMmDb5mEay_VijI" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Municipalidad-Distrital-de-Paij%C3%A1n-1962109457238478/?__tn__=kCH-R&amp;eid=ARBdg-UjMFiLtTuL7jErFRDQt11D1xfMm5Oau8-dLfqS9r_Nex9SxqxEYLbxGwIwuXZe6tB12pnl8_sh&amp;hc_ref=ARTMzZ0r8qVgbDWtNzpTVrsd-9eh4V34lldYCJPyVeY8AEDZqfKqU8ctP9052lI8oik&amp;fref=nf&amp;__xts__[0]=68.ARB3nvOsovF47JaQ8ou507VDEgNRjRA7MmZDE50LR1EG-Qw1iYTprggAMgt0a_YI5ahKvktvJpumoMQBZbW1cWJgYO5ycBle2_HRPz65S-wfTwslSJmbjU7VJQWuFzC7t3Q3zOCAnxIWN_655-br_s8tIcrrhvgq2BkemlaIG2tclIoIwymQyG7CK_vFIeVopy4VWGGuKenp5E-HjFfuE35Rz57HM_fDCLnJ0gi2yW4JbsM3vGyfy2UPebuMaRwgOUQRSVV-GdWzoDmiz8bj22Y3FnT9OpAM5J0LIOAxbdtr9JlNKdjAnkaTOo6lfbeYxU06CbYlraHmKMmDb5mEay_VijI" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Municipalidad-Distrital-de-Paij%C3%A1n-1962109457238478/?__tn__=kCH-R&amp;eid=ARBdg-UjMFiLtTuL7jErFRDQt11D1xfMm5Oau8-dLfqS9r_Nex9SxqxEYLbxGwIwuXZe6tB12pnl8_sh&amp;hc_ref=ARTMzZ0r8qVgbDWtNzpTVrsd-9eh4V34lldYCJPyVeY8AEDZqfKqU8ctP9052lI8oik&amp;fref=nf&amp;__xts__[0]=68.ARB3nvOsovF47JaQ8ou507VDEgNRjRA7MmZDE50LR1EG-Qw1iYTprggAMgt0a_YI5ahKvktvJpumoMQBZbW1cWJgYO5ycBle2_HRPz65S-wfTwslSJmbjU7VJQWuFzC7t3Q3zOCAnxIWN_655-br_s8tIcrrhvgq2BkemlaIG2tclIoIwymQyG7CK_vFIeVopy4VWGGuKenp5E-HjFfuE35Rz57HM_fDCLnJ0gi2yW4JbsM3vGyfy2UPebuMaRwgOUQRSVV-GdWzoDmiz8bj22Y3FnT9OpAM5J0LIOAxbdtr9JlNKdjAnkaTOo6lfbeYxU06CbYlraHmKMmDb5mEay_VijI">Municipalidad Distrital de Paiján</a></blockquote></div></center>
							</div>
						</div>							
					
						<!--<div class="col-md-12">
							<div class="city_service2_btn btn2">
								<a class="theam_btn border-color color" href="#" tabindex="0">See More</a>
							</div>
						</div>-->
					</div>
				</div>
			</div>	
			<!--CITY ABOUT WRAP START-->
			
			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->

			<?php include 'footer.php'; ?>
		
		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/index.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
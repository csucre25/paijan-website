<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bolsa de Trabajo - Municipalidad de Paiján</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 
			
			<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Bolsa de Trabajo</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Home</a></li>
						  <li class="breadcrumb-item active"><a href="bolsa.php">Bolsa de trabajo</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			
			<!-- CITY EVENT2 WRAP START-->
			<div class="city_blog2_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="city_blog2_fig fig2 responsive">
								<div class="city_blog2_list">
										<span class="city_blog2_met">VIGENTE - 3ª CONVOCATORIA</span>
									<div class="city_blog2_text">
										<h4><a href="#">JEFE DE LA UNIDAD DE TECNOLOGÍA Y SISTEMAS INFORMÁTICOS</a></h4>
										<p>La Municipalidad Distrital de Paiján, requiere contratar bajo el Régimen Especial de Contrato Administrativo de Servicios, Regulado por el Decreto Legislativo N° 1057 a un JEFE DE LA UNIDAD DE TECNOLOGÍA Y SISTEMAS INFORMÁTICOS- MUNICIPALIDAD DISTRITAL DE PAIJAN, con el fin de brindar, Formular, gestionar su aprobación e implementar el Plan de Transición al Protocolo IPV6...</p>
									</div>
									<ul class="city_meta_list">
										<li><a href="#"><i class="fa fa-calendar"></i>27 de noviembre al 14 de diciembre</a></li>
										<!--<li><a href="#"><i class="fa fa-comment-o"></i>0 Comments</a></li>-->
									</ul>
									<a class="theam_btn border-color color" href="#" tabindex="0">Ver más...</a>
								</div>
							</div>
							<div class="city_blog2_fig fig2 responsive">
								<div class="city_blog2_list">
										<span class="city_blog2_met">FINALIZADO</span>
									<div class="city_blog2_text">
										<h4><a href="#">SUBGERENTE DE SEGURIDAD CIUDADANA</a></h4>
										<p>La Municipalidad Distrital de Paiján, requiere contratar bajo el Régimen Especial de Contrato Administrativo de Servicios, Regulado por el Decreto Legislativo N° 1057 a un SUBGERENTE DE SEGURIDAD CIUDADANA - MUNICIPALIDAD DISTRITAL DE PAIJAN, con el fin de dirigir, promover y ejecutar la política nacional, regional, provincial y local de seguridad ciudadana en coordinación con la Policía Nacional del Perú, instituciones públicas y privadas y juntas vecinales de seguridad ciudadana...</p>
									</div>
									<ul class="city_meta_list">
										<li><a href="#"><i class="fa fa-calendar"></i>24 de noviembre al 08 de diciembre</a></li>
										<!--<li><a href="#"><i class="fa fa-comment-o"></i>0 Comments</a></li>-->
									</ul>
									<a class="theam_btn border-color color" href="#" tabindex="0">Ver más...</a>
								</div>
							</div>
							<div class="city_blog2_fig fig2 responsive">
								<div class="city_blog2_list">
										<span class="city_blog2_met">FINALIZADO - 2ª CONVOCATORIA</span>
									<div class="city_blog2_text">
										<h4><a href="#">JEFE DE LA UNIDAD DE TECNOLOGÍA Y SISTEMAS INFORMÁTICOS</a></h4>
										<p>La Municipalidad Distrital de Paiján, requiere contratar bajo el Régimen Especial de Contrato Administrativo de Servicios, Regulado por el Decreto Legislativo N° 1057 a un JEFE DE LA UNIDAD DE TECNOLOGÍA Y SISTEMAS INFORMÁTICOS- MUNICIPALIDAD DISTRITAL DE PAIJAN, con el fin de brindar, Formular, gestionar su aprobación e implementar el Plan de Transición al Protocolo IPV6...</p>
									</div>
									<ul class="city_meta_list">
										<li><a href="#"><i class="fa fa-calendar"></i>06 de noviembre al 23 de noviembre</a></li>
										<!--<li><a href="#"><i class="fa fa-comment-o"></i>0 Comments</a></li>-->
									</ul>
									<a class="theam_btn border-color color" href="#" tabindex="0">Ver más...</a>
								</div>
							</div>
							<div class="city_blog2_fig fig2 responsive">
								<div class="city_blog2_list">
										<span class="city_blog2_met">FINALIZADO</span>
									<div class="city_blog2_text">
										<h4><a href="#">JEFE DE LA UNIDAD DE TECNOLOGÍA Y SISTEMAS INFORMÁTICOS</a></h4>
										<p>La Municipalidad Distrital de Paiján, requiere contratar bajo el Régimen Especial de Contrato Administrativo de Servicios, Regulado por el Decreto Legislativo N° 1057 a un JEFE DE LA UNIDAD DE TECNOLOGÍA Y SISTEMAS INFORMÁTICOS- MUNICIPALIDAD DISTRITAL DE PAIJAN, con el fin de brindar, Formular, gestionar su aprobación e implementar el Plan de Transición al Protocolo IPV6...</p>
									</div>
									<ul class="city_meta_list">
										<li><a href="#"><i class="fa fa-calendar"></i>20 de octubre al 03 de noviembre</a></li>
										<!--<li><a href="#"><i class="fa fa-comment-o"></i>0 Comments</a></li>-->
									</ul>
									<a class="theam_btn border-color color" href="#" tabindex="0">Ver más...</a>
								</div>
							</div>
							<div class="city_blog2_fig fig2 responsive">
								<div class="city_blog2_list">
										<span class="city_blog2_met">FINALIZADO</span>
									<div class="city_blog2_text">
										<h4><a href="#">GESTOR EN CONTRATACIONES PARA LA EJECUCIÓN DE INVERSIONES</a></h4>
										<p>La Municipalidad Distrital de Paiján, requiere contratar bajo el Régimen Especial de Contrato Administrativo de Servicios, Regulado por el Decreto Legislativo N° 1057 a un GESTOR EN CONTRATACIONES PARA LA EJECUCIÓN DE INVERSIONES- MUNICIPALIDAD DISTRITAL DE PAIJAN...</p>
									</div>
									<ul class="city_meta_list">
										<li><a href="#"><i class="fa fa-calendar"></i>17 de setiembre al 01 de octubre</a></li>
										<!--<li><a href="#"><i class="fa fa-comment-o"></i>0 Comments</a></li>-->
									</ul>
									<a class="theam_btn border-color color" href="#" tabindex="0">Ver más...</a>
								</div>
							</div>
							<div class="city_blog2_fig fig2 responsive">
								<div class="city_blog2_list">
										<span class="city_blog2_met">FINALIZADO</span>
									<div class="city_blog2_text">
										<h4><a href="#">GESTOR EN CONTRATACIONES PARA LA EJECUCIÓN DE INVERSIONES</a></h4>
										<p>La Municipalidad Distrital de Paiján, requiere contratar bajo el Régimen Especial de Contrato Administrativo de Servicios, Regulado por el Decreto Legislativo N° 1057 y reglamento por el Decreto Supremo N° 075-2008-PCM, Decreto Supremo N° 065-2011-PCM-D.S...</p>
									</div>
									<ul class="city_meta_list">
										<li><a href="#"><i class="fa fa-calendar"></i>12 de agosto al 26 de agosto</a></li>
										<!--<li><a href="#"><i class="fa fa-comment-o"></i>0 Comments</a></li>-->
									</ul>
									<a class="theam_btn border-color color" href="#" tabindex="0">Ver más...</a>
								</div>
							</div>
							
							<!--<div class="pagination">
								<ul>
									<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
									<li><a href="#">01</a></li>
									<li><a href="#">02</a></li>
									<li><a href="#">....</a></li>
									<li><a href="#">08</a></li>
									<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
								</ul>
							</div>-->
						</div>
					</div>
				</div>
			</div>
			<!-- CITY EVENT2 WRAP END-->
			
			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap requst02">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->
			
			<?php include 'footer.php'; ?>

		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
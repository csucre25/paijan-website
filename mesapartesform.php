<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mesa de partes virtual</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
		<link href="css/tailwind.min.css" rel="stylesheet">
		
		<style>
        input[type=radio]+label span {
            transition: background .2s, transform .2s
        }

        .fileContainer:hover {
            background-color: #ebb300
        }

        label.error {
            color: #f56565;
            font-size: .75rem;
            font-style: italic
        }

        input[type=radio]+label span:hover,
        input[type=radio]+label:hover span {
            transform: scale(1.2)
        }

        input[type=radio]:checked+label span {
            background-color: #4299e1;
            box-shadow: 0 0 0 2px #fff inset
        }

        input[type=radio]:checked+label {
            color: #4299e1
        }

        #loader {
            display: none
        }

        .fileContainer {
            overflow: hidden;
            position: relative;
        }

        .fileContainer [type=file] {
            cursor: inherit;
            display: block;
            font-size: 999px;
            filter: alpha(opacity=0);
            min-height: 100%;
            min-width: 100%;
            opacity: 0;
            position: absolute;
            right: 0;
            text-align: right;
            top: 0;
        }

        .fileContainer {
            background: #f5bd08;
            border-radius: .5em;
            float: left;
            padding: .5em;
            color: white;
            margin-bottom: 1rem;
        }

        .fileContainer [type=file] {
            cursor: pointer;
        }
    </style>
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 

		<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Mesa de partes</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Inicio</a></li>
						  <li class="breadcrumb-item active"><a href="mesapartes.php">Mesa de partes</a></li>						  
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			
			<!-- CITY SERVICES2 WRAP START-->
			<div class="city_health_department">
				<div class="container">
					<div class="row">						
						<div class="col-md-12">							
								<!--SECTION HEADING END-->
    							<div class="container mx-auto">
        							<div class="sticky top-0 bg-white z-50">
            							<div class="text-right pt-2 mr-3">
											<span class="text-gray-700 mr-3">
                    							contacto@munipaijan.gob.pe
											</span> 
										</div>
            							<h1 class="my-3 block uppercase text-center text-white bg-blue-700 p-4">
											<span class="font-bold">UNIDAD DE TRÁMITE DOCUMENTARIO</span> - MESA DE PARTES VIRTUAL MDP - en mantenimiento
            							</h1>
										<h5 style align="justify"> La Municipalidad Distrital de la Paiján, con el objetivo de garantizar la continuidad de la gestión institucional, priorizando el trabajo remoto y la atención a la ciudadanía de manera no presencial, con el ojetivo de seguir vigilantes ante la emergencia sanitaria a consecuencia del COVID-19, ha implementado a través de la Unidad de Trámite Documentario, este formulario de registro virtual de trámites documentarios<br><br>
											A continuación deberá registrar todos los datos requeridos y adjuntar los documentos necesarios en forma legible.
										</h5>
       								</div>

        							<form id="formulario" name="formulario" class="my-4 w-full p-4 border-dashed border-gray-200 border-2"  method="post" enctype="multipart/form-data" action="enviarmesa2.php">
            							<h2 class="w-full px-3 py-1 bg-blue-500 mb-2 text-white flex items-center font-medium">
            							    01. DATOS PERSONALES DEL REMITENTE
           								</h2>

							            <div class="w-full md:mb-6 md:mb-0">
                							<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                							    TIPO DE PERSONA
                							</label>
                							<div class="relative">
                							    <select  id="tipo_persona" name="tipo_persona" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white ">
													<option option disabled=""  selected="selected">Seleccionar</option>  
                							        <option id="Natural" value="Natural">Natural</option>
                							        <option id="Persona Juridica" value="Persona Juridica">Persona Juridica</option>
                							    </select>

                							</div>
           								</div>
            							<div>            
                							<div class="flex flex-wrap -mx-3 mb-6">
                							    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                							        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                							            Nombres Completos
                							        </label>
                							        <input autocomplete="off" id="nombres" name="nombres" class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" type="text" minlength="1" >
                							    </div>
                							    <div class="w-full md:w-1/2 px-3">
                							        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="apellidos">
                							            Apellidos Completos
                							        </label>

                							        <input autocomplete="off" id="apellidos" name="apellidos" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" >
                							    </div>
                							</div>
            							</div>
            							<div id="section_juridica" style="display: none;">            
            							    <div class="flex flex-wrap -mx-3 mb-6">
            							        <div class="w-full px-3 mb-6 md:mb-0">
            							            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            							                RAZÓN SOCIAL
            							            </label>
            							            <input autocomplete="off" id="razonsocial" name="razonsocial" class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" type="text" minlength="1">
            							        </div>
            							    </div>
            							</div>
							            <div class="flex flex-wrap -mx-3 mb-2">

            							    <div class="w-full md:w-1/1 px-3 mb-6 md:mb-0">
            							        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            							            Dirección
            							        </label>

            							        <input autocomplete="off" id="direccion" name="direccion" class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" type="text" minlength="1">

            							    </div>
				
											<div class="w-full md:w-1/4 px-3 mb-6 md:mb-0">
            							        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            							            Tipo de Documentos
            							        </label>
					
            							        <select  id="tipo_Identificacion" name="tipo_Identificacion" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white ">
													<option option disabled=""  selected="selected">Seleccionar</option>  
            							            <option id="dni" value="DNI">DNI</option>
            							            <option id="RUC" value="RUC">RUC</option>
													<option id="pasaporte" value="Pasaporte">Pasaporte</option>
            							        </select>

            							    </div>
											<div class="w-full md:w-1/4 px-3 mb-6 md:mb-0">
            							        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            							            Nro.
            							        </label>
					
            							        <input autocomplete="off" type="input" id="numero" name="numero" class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" minlength="1" maxlength="9">

            							    </div>
            							    <div class="w-full md:w-1/4 px-3 mb-6 md:mb-0">
            							        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            							            Correo Electronico
            							        </label>
            							        <input autocomplete="off" id="correo" name="correo" class="appearance-none block w-full bg-gray-200 text-gray-900 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" type="text">

            							    </div>
            							    <div class="w-full md:w-1/4 px-3 mb-6 md:mb-0">
            							        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            							            Número de Celular o Teléfono
            							        </label>
            							        <input autocomplete="off" type="input" id="celular" name="celular" class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" minlength="1" maxlength="9">
            							    </div>
            							</div>

            							<h2 class="w-full px-3 py-1 bg-blue-500 mb-2 text-white flex items-center font-medium">
            							    02. DATOS DEL DOCUMENTO
            							</h2>
										
										<div class="flex flex-wrap -mx-3 mb-2">

											<div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
												<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
													Tipo de Documento
												</label>
												<div class="relative">
													<select name="tipo_documento" id="tipo_documento" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
														<option option disabled=""  selected="selected">Seleccionar</option>  

															<option value=" ACTA DE CONSTATACION ">ACTA DE CONSTATACION    </option>      
															<option value=" AVISO ">AVISO    </option>      
															<option value=" AVISO REGISTRAL ">AVISO REGISTRAL    </option>      
															<option value=" CARTA ">CARTA    </option>      
															<option value=" CARTA MULTIPLE ">CARTA MULTIPLE    </option>      
															<option value=" CARTA NOTARIAL ">CARTA NOTARIAL    </option>      
															<option value=" CEDULA DE NOTIFICACION ">CEDULA DE NOTIFICACION    </option>      
															<option value=" CITACION ">CITACION    </option>      
															<option value=" CONSTANCIA ">CONSTANCIA    </option>      
															<option value=" COTIZACION ">COTIZACION    </option>      
															<option value=" DECRETOS ">DECRETOS    </option>      
															<option value=" DENUNCIA ">DENUNCIA    </option>      
															<option value=" DESCARGO ">DESCARGO    </option>      
															<option value=" EXPEDIENTE ">EXPEDIENTE    </option>      
															<option value=" INFORME ">INFORME    </option>      
															<option value=" INFORME LEGAL ">INFORME LEGAL    </option>      
															<option value=" INVITACION ">INVITACION    </option>      
															<option value=" MEMORANDUM ">MEMORANDUM    </option>      
															<option value=" MEMORANDUM MULTIPLE ">MEMORANDUM MULTIPLE    </option>      
															<option value=" MEMORIAL ">	MEMORIAL    </option>      
															<option value=" NOTIFICACION ">NOTIFICACION    </option>      
															<option value=" OFICIO ">OFICIO    </option>      
															<option value=" OFICIO MULTIPLE ">OFICIO MULTIPLE    </option>      
															<option value=" PEDIDO ">PEDIDO    </option>      
															<option value=" PROVEIDO ">PROVEIDO    </option>      
															<option value=" RESOLUCION ">RESOLUCION    </option>      
															<option value=" SOLICITUD ">OLICITUD    </option>      
															<option value=" SOLICITUD DE ACCESO DE LA INFORMACIÓN PÚBLICA ">SOLICITUD DE ACCESO DE LA INFORMACIÓN PÚBLICA    </option>     
															<option value=" OTRO ">OTRO    </option> 
													</select>
													<div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
														<svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
															<path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"></path>
														</svg>
													</div>
												</div>

											</div>
											<div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
												<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
													Numero de Documento
												</label>
												<input autocomplete="off" id="ndocumento" name="ndocumento" class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" type="text">

											</div>
											<div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
												<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
													Número de Folios
												</label>
												<input autocomplete="off" type="input" id="folios" name="folios" class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" minlength="1">
											</div>
											<div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
												<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
													ASUNTO DEL DOCUMENTO
												</label>
												<input autocomplete="off" type="input" id="asunto" name="asunto" class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" minlength="1">
											</div>
										</div>
			
										<div class="w-full mb-3">
											<div class="border rounded flex flex-col bg-white">
												<div class="bg-grey-lighter px-3 py-2 border-b">
													<h3 class="uppercase tracking-wide text-gray-700 text-xs font-bold">DESCRIPCION DEL DOCUMENTO</h3>
												</div>
												<textarea id="descripcion" name="descripcion" class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white"></textarea>
											</div>
										</div>

										<div class="overflow-hidden w-full  mb-6 md:mb-0 relative pt-1">
											<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
												DOCUMENTO
											</label>
											<div class="flex mb-2 items-center justify-between">
												<div>
													<span id="estado" class="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-gray-600 bg-gray-200">
														SIN ARCHIVO
													</span>
													<label id="namearchivo"></label>
												</div>
												<div class="text-right">
													<span id="barra-numero" class="text-xs font-semibold inline-block text-blue-600">
														0%
													</span>
												</div>
											</div>
											<div id="base" class="overflow-hidden h-2 mb-4 text-xs flex rounded bg-gray-200">
												<div id="barra-linea" style="width:0%" class="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-blue-500">
												</div>
											</div>
											<label class="fileContainer">
												Seleccionar Archivo
												<input autocomplete="off" required="" id="btn-archivo" name="archivo" type="file">
											</label>

										</div>

										<div class="mb-5 bg-blue-100 border-t-4 border-blue-500 rounded-b text-blue-900 px-4 py-3 shadow-md" role="alert">
											<div class="flex">
												<div class="py-1"><svg class="fill-current h-6 w-6 text-blue-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
														<path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"></path>
													</svg></div>
												<div>
													<p class="font-bold">NOTA:</p>
													<p class="text-sm">Horario de recepción de Lunes a Viernes de 8:00 AM - 3:00 PM.</p>
													<p class="text-sm">	El documento principal debe estar en formato pdf<BR>
														Para documentos anexos el formato puede ser pdf , jpg , png y word<BR>
														Los archivos a adjuntar no deben superar los 30 Mb en forma individual o entre todos los adjuntos</p>
												</div>
											</div>
										</div>

										<div id="info"></div>
										<div class="text-center">
											<button type="submit" class="shadow bg-blue-700 hover:bg-blue-400 focus:shadow-outline focus:outline-none text-white font-600 py-2 px-4 rounded">
												REGISTRAR EXPEDIENTE
											</button>
										</div>
	      						    </form>
	    						</div>
						</div>
					</div>
				</div>	
			</div>
			<!-- CITY SERVICES2 WRAP END-->

			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->
            <?php include 'footer.php'; ?>
		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
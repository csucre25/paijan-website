<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Licencia de edificación</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 

		<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Licencia de edificación</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Inicio</a></li>
						  <li class="breadcrumb-item active"><a href="licenciaedificacion.php">Licencia de edificación</a></li>						  
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			
			<!-- CITY SERVICES2 WRAP START-->
			<div class="city_health_department">
				<div class="container">
					<div class="row">						
						<div class="col-md-12">
							<div class="city_about_list list2">
								<!--SECTION HEADING START-->
								<div class="section_heading">
									<span>Información de</span>
									<h2>Licencia de edificación</h2>
								</div>
								<!--SECTION HEADING END-->
								<div class="city_about_text ">
									<h6>Requisitos:</h6>
                                   
                                    <p><b>SUB DIVISION DE LOTE URBANO</b></p>
                                    <p>1. FUHU por triplicado y debidamente suscritos.
                                    </p>
                                    <p>2. Copia literal de dominio expedida por el Registro de Predios, con una antiguedad no mayor a treinta(30) días calendario.
                                    </p>
                                    <p>3. En caso que el administrativo no sea el propietario del predio, la escritura que acredite el derecho a habilitar.
                                    </p>
                                    <p>4. En caso el solicitante sea una persona jurídica, se acompañara vigencia del poder expedida por el Registro de Personas Jurídicas, con una antiguedad no mayor de treinta (30) días naturales.
                                    </p>
                                    <p>5. Declaración Jurada de habilitación de los profesionales que intervienen en el proyecto y suscriben la documentación técnica.
                                    </p>
                                    <p>6. Copia del comprobante de pago de la tasa municipal correspondiente.
                                    </p>
                                    <p><b>7. DOCUMENTACIÓN TÉCNICA</b>
                                    </p>
                                    <p>- Plano de Ubicación y Localización del lote materia de Subdivisión.
                                    </p>                                    
                                    <p>- Plano de la Subdivisión, señalando areas, linderos, medidas perimétricas y nomenclatura de cada sublote propuesto resultante.
                                    </p>
                                    <p>- Memoria Descriptiva, indicando áreas, linderos y medidas perimétricas del lote de subdivisión y de los sublotes propuestos resultantes.
                                    </p>
                                    <p>- FUHU
                                    </p>
                                    </br>
                                    <p>(a) El formulario y sus anexos deben ser visado en todas sus páginas y cuando corresponda, firmados por el propietario o por el solicitante y los profesionales que interviene.
                                    </p>
                                    <p>(b) Todos los planos y documentos técnicos deben estar sellados y firmados por el profesional responsable de los mismos y firmados por el propietario o solicitante.
                                    </p>            
                                    <p>(c) En caso se solicite la subdivisión de un lote que cuente con obras de Habilitación Urbana inconclusas, dichas obras deberán ser ejecutadas y recepcionadas en el mismo procedimiento.
                                    </p>   

                                    </br>
                                    <p><b>CONSTANCIA DE POSESIÓN</b>
                                    </p>
                                    <p>1. Solicitud simple indicando nombre, dirección y número de DNI.
                                    </p>
                                    <p>2. Copia del DNI del solicitante.
                                    </p>
                                    <p>3. Documento que acrediten ser dueño de dicha propiedad ( Minuta, Contrato de Traspaso, Herencia, Compra-venta, Documento emitido por un juez, Constancia de Posesión antigua).
                                    </p>
                                    <p>4. Plano simple de Ubicación del lote (Si lo tuviera).
                                    </p>
                                    <p>5. Pago por derecho de trámite en Caja. S/. 31.00
                                    </p>
                                    </br>
                                    <p>Se otorga Constancia de Posesión solo para el otorgamiento de la Factibilidad de Servicios Básicos (Luz Eléctrica, Agua y Desague) de acuerdo al Titulo III del DS Nº017-2006-VIVIENDA, el mismo que no constituye reconomiento alguno que afecte el derecho de propiedad de su titular.
                                    </p>
                                    </br>
                                    <p><b>CERTIFICADO DE NUMERACIÓN DE FINCA</b>
                                    </p>
                                    <p>1. Solicitud simple, firmada por el propietario o representante del titular con carta poder simple o representante legal acreditado para el caso de personas jurídicas.
                                    </p>
                                    <p>2. Copia literal del dominio actualizada y otros documentos de propiedad del inmueble de ser el caso (Contrato de Compra - Venta, Minuta o Testimonio)
                                    </p>
                                    <p>3. Planos de Ubicación.
                                    </p>
                                    <p>4. Consignar en la solicitud fecha y numero del ultimo recibo de Pago del impuesto predial cancelado (AUTOVALUO), y/o adjuntar copia del mismo fedeteado.
                                    </p>
                                    <p>5. Pago por derecho de trámite S/.22.00 (Ley 29090)
                                    </p>
                                </div>
                                    
                                
                               
							</div>
						</div>
					</div>
				</div>	
			</div>
			<!-- CITY SERVICES2 WRAP END-->

			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->
            <?php include 'footer.php'; ?>
		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
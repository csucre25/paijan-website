<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Vacunación - Municipalidad de Paiján</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 
			
			<?php include 'top.php'; ?>

			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Obras</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Home</a></li>
						  <li class="breadcrumb-item"><a href="obras.php">Obras</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			
						
			<!-- CITY TREATMENT WRAP START-->
			<div class="city_treatment_wrap">
				<div class="container">
					<ul class="bxslider bx-pager">
						<li>
							<div class="city_treatment_fig">
								<figure class="overlay">
									<img src="images/obras/obra1.jpeg" alt="">
									<div class="city_treatment_text">
										<a class="paly_btn" data-rel="prettyPhoto" href="images/obras/obra4.jpeg"><i class="fa fa-eye"></i></a>
										<h4>Supervisión de Obras</h4>
										<!--h5>Aenean sollicitudin, lorem</h5-->
									</div>
								</figure>
							</div>
						</li>
						<li>
							<div class="city_treatment_fig">
								<figure class="overlay">
									<img src="images/obras/obra2.jpeg" alt="">
									<div class="city_treatment_text">
										<a class="paly_btn" data-rel="prettyPhoto" href="images/obras/obra2.jpeg"><i class="fa fa-eye"></i></a>
										<h4>Supervisión de Obras</h4>
										<!--h5>Aenean sollicitudin, lorem</h5-->
									</div>
								</figure>
							</div>
						</li>
						<li>
							<div class="city_treatment_fig">
								<figure class="overlay">
									<img src="images/obras/obra3.jpeg" alt="">
									<div class="city_treatment_text">
										<a class="paly_btn" data-rel="prettyPhoto" href="images/obras/obra3.jpeg"><i class="fa fa-eye"></i></a>
										<h4>Supervisión de Obras</h4>
										<!--h5>Aenean sollicitudin, lorem</h5-->
									</div>
								</figure>
							</div>
						</li>
						<li>
							<div class="city_treatment_fig">
								<figure class="overlay">
									<img src="images/obras/obra4.jpeg" alt="">
									<div class="city_treatment_text">
										<a class="paly_btn" data-rel="prettyPhoto" href="images/obras/obra4.jpeg"><i class="fa fa-eye"></i></a>
										<h4>Supervisión de Obras</h4>
										<!--h5>Aenean sollicitudin, lorem</h5-->
									</div>
								</figure>
							</div>
						</li>		
					</ul>	
					<div id="bx-pager" class="city_treatment_list">
						<a class="overlay" data-slide-index="0" href=""><img src="images/obras/obra1.jpeg" alt=""></a>
						<a class="overlay" data-slide-index="1" href=""><img src="images/obras/obra2.jpeg" alt=""></a>
						<a class="overlay" data-slide-index="2" href=""><img src="images/obras/obra3.jpeg" alt=""></a>
						<a class="overlay" data-slide-index="3" href=""><img src="images/obras/obra4.jpeg" alt=""></a>
					</div>
				</div>
			</div>
			<!-- CITY TREATMENT WRAP END-->			
			
			<!--CITY AWARD WRAP START-->
			<!--<div class="city_award_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4">
							<div class="city_award_list">
								<span><i class="fa  icon-politician"></i></span>
								<div class="city_award_text">
									<h3 class="counter">1495</h3>
									<h3>Established</h3>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="city_award_list">
								<span><i class="fa icon-cube"></i></span>
								<div class="city_award_text">
									<h3 class="counter">75,399</h3>
									<h3>KM Square</h3>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="city_award_list">
								<span><i class="fa icon-demographics"></i></span>
								<div class="city_award_text">
									<h3 class="counter">1,435,268</h3>
									<h3>Total Population</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>-->
			<!--CITY AWARD WRAP END-->
			
			<!--CITY PROJECT WRAP START-->
			<div class="city_project_wrap">
				<div class="container">
					<!--SECTION HEADING START-->
					<div class="section_heading center margin-bottom">
						<span>Municipalidad de Paíjan</span>
						<h2>Obras</h2>
					</div>
					<div class="city-project-slider">
					<div>
							<div class="city_project_fig">
								<figure class="overlay">
									<img src="extra-images/program_fig.jpg" alt="">
									<div class="city_project_text">
										<span><i class="fa icon-immune-system"></i></span>
										<a href="obras.php">Obras</a>
										<h3><a href="obras.php">Obras</a></h3>
									</div>
								</figure>
							</div>
						</div>
					
						<div>
							<div class="city_project_fig">
								<figure class="overlay">
									<img src="extra-images/program_fig1.jpg" alt="">
									<div class="city_project_text">
										<span><i class="fa icon-immune-system"></i></span>
										<a href="mejoraobras.php">Obras</a>
										<h3><a href="mejoraobras.php">Mejoramiento de obras</h3>
									</div>
								</figure>
							</div>
						</div>
						
						<div>
							<div class="city_project_fig">
								<figure class="overlay">
									<img src="extra-images/program_fig2.jpg" alt="">
									<div class="city_project_text">
										<span><i class="fa icon-immune-system"></i></span>
										<a href="mejoracolegio.php">Obras</a>
										<h3><a href="mejoracolegio.php">Mejoramiento de Colegio Pilar Nores</a></h3>
									</div>
								</figure>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY PROJECT WRAP END-->
			
			<?php include 'footer.php'; ?>

		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
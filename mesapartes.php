<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mesa de partes virtual</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 

		<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Mesa de partes</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Inicio</a></li>
						  <li class="breadcrumb-item active"><a href="mesapartes.php">Mesa de partes</a></li>						  
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			
			<!-- CITY SERVICES2 WRAP START-->
			<div class="city_health_department">
				<div class="container">
					<div class="row">						
						<div class="col-md-12">
							<div class="city_about_list list2">
								<!--SECTION HEADING START-->
								<div class="section_heading">
									<span>Bienvenido a</span>
									<h2>Mesa de partes virtual</h2>
								</div>
								<!--SECTION HEADING END-->
								<div class="city_about_text ">
									<h6>Atención: Lunes a Viernes de 8:00 am a 3:00 pm | Teléfono: xxxxxx | Email: mesadepartes@munipaijan.gob.pe</h6>

                                
									<p>Mediante <a href = "decretosupremo044_2020.php">Decreto Supremo Nº 044-2020-PCM</a>, precisado por los <a href = "decretosupremo045_2020.php">Decretos Supremos Nº 045-2020-PCM</a> y <a href = "decretosupremo046_2020.php">046-2020-PCM</a>, se declara el Estado de Emergencia Nacional y se dispone el aislamiento social obligatorio (cuarentena), por las graves circunstancias que afectan la vida de la Nación a consecuencia del COVID-19; habiéndose prorrogado dicho plazo por los <a href = "decretosupremo051_2020.php">Decretos Supremos Nº 051-2020-PCM</a>, <a href = "decretosupremo064_2020.php">064-2020-PCM</a> y <a href = "decretosupremo075_2020.php">075-2020-PCM</a>, buscando salvaguardar la vida y salud de los peruanos, por ello la Municipalidad Distrital de la Paiján, pone a su dispocición la Mesa de Partes Virtual, donde podrá realizar sus trámites documentarios.
                                    Los datos y la documentación que Ud. ingrese llegarán directamente al personal de la oficina de la Unidad de Trámite Documentario, quienes luego de la validación de requisitos en primera instancia, le enviarán la respuesta correspondiente al registro de su solicitud. Para ellos debe tener en cuenta lo siguiente:
                                    El horario de recepción de documentos es de Lunes a Viernes de 8:00am a 3:00pm. Fuera de este horario, la documentación se recepcionará el siguiente día útil, una vez validado el cumplimiento de requisitos.
                                    Antes de registrar su trámite, deberá verificar los requisitos solicitados por cada trámite.
                                    Si el trámite consta de varios requisitos, toda la documentación anexada deberá ser escaneada y consolidada en un solo archivo que no supere los 15MB. Además la información y anexos presentados deben ser legibles.
                                    Si el trámite registrado no cumple con los requisitos solicitados o algún requisito requiere necesariamente de su presentación en físico, se le notificará mediante correo electrónico.
                                    Una vez recepcionado el trámite, se enviará al administrado un correo electrónico de confirmación.
                                    Para cualquier consulta adicional, o presentarse algún inconveniente enviar un correo electrónico a mesadepartes@munipaijan.gob.pe o comunicarse al teléfono xxx xxxs.
                                    Los datos registrados en esta solicitud tiene carácter de declaración jurada, por lo que están sujetos a fiscalización posterior. <a>Haga Clic en la Imágen para continuar...</a> 
                                    </p>	
                                </div>
                                    
                                <center><a href="mesapartesform.php"><img src="images/MESAVIRTUAL.svg" width="150px"></center>
                               
							</div>
						</div>
					</div>
				</div>	
			</div>
			<!-- CITY SERVICES2 WRAP END-->

			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->
            <?php include 'footer.php'; ?>
		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
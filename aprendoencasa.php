<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Aprendo en casa</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 

		<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Aprendo en casa</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Inicio</a></li>
						  <li class="breadcrumb-item active"><a href="aprendoencasa.php">Aprendo en casa</a></li>						  
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			
			<!-- CITY SERVICES2 WRAP START-->
			<div class="city_health_department">
				<div class="container">
					<div class="row">						
						<div class="col-md-12">
							<div class="city_about_list list2">
								<!--SECTION HEADING START-->
								<div class="section_heading">
									<span>Bienvenido a</span>
									<h2>Aprendo en casa</h2>
								</div>
								<!--SECTION HEADING END-->
								<div class="city_about_text ">
									<h6>Nota de prensa</h6>

                                    <p>Es una plataforma de educación que se actualiza constantemente con contenidos de calidad que ayuda a los estudiantes de los niveles de inicial, primaria y secundaria. Es importante porque llega a todos los peruanos para que puedas ser una ayuda complementaria para los profesores, especialmente en zonas rurales del país. Por último, el objetivo principal es reducir la brecha educacional que seguimos teniendo en el Perú.</p>
                                    </p>
                                    </br>
                                    <p><b>¿Cómo Mejorara la Enseñanza?</b></p>
                                    <p>La educación es importante para el desarrollo del país, y la innovación de la plataforma es buena por eso la convierte en un proyecto muy importante que ha recibido el apoyo de diferentes empresas tanto dentro como fuera del país para que la llegada en la población sea mucho más rápida.
                                    </p>
                                    </br>
                                    <p>El Ministerio de Transporte y Comunicaciones también ha realizado convenios para la ampliación de cobertura. Estos tratados se han realizado con las empresas de telecomunicaciones para que al acceder al sitio web no consuma datos en la población y se aumente la potencia de transmisión. Asimismo, se han brindado los permisos al Instituto de Radio y Televisión del Perú para dar el servicio de radiodifusión por satélites para las poblaciones más alejadas.
                                    </p>
                                    </br>
                                    <p>Los medios que transmiten esta plataforma son la televisión, la radio y el internet. El Ministerio de Transporte y Comunicaciones, se está encargando de que cada vez más plataformas de comunicación se sumen a la plataforma.
                                    </p>
                                    </br>
                                    <p><b>¿Qué Herramientas usa?</b></p>
                                    <p>La plataforma tiene materiales interesantes como videos, cuadernos de desarrollo, documentos relevantes, juegos educativos, documentales, entre otros. Todos estos materiales ayudan al estudiante a poder tener un mejor aprendizaje.
                                    </p>
                                    </br>
                                    <p><b>MÁS INFORMACIÓN:</b></p>
                                    <p><a href="https://resources.aprendoencasa.pe/perueduca/orientaciones/familia/familia-orientaciones-que-es-aprendo-en-casa.pdf">https://resources.aprendoencasa.pe/perueduca/orientaciones/familia/familia-orientaciones-que-es-aprendo-en-casa.pdf</a></p>
                                    </br>
                                    <p><b>Fuente: </b><a href="https://selectra.com.pe/info/aprendo-en-casa">https://selectra.com.pe/info/aprendo-en-casa</a></p>	
                                    </br>
                                    <p>Click en la imagen para ingresar...</p>
                                </div>
                                    
                                <center><a href="https://aprendoencasa.pe/#/"><img src="images/aprendoencasa.jpg" width="300px"></center>
                               
							</div>
						</div>
					</div>
				</div>	
			</div>
			<!-- CITY SERVICES2 WRAP END-->

			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->
            <?php include 'footer.php'; ?>
		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>
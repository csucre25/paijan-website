<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tu Municipalidad - - Municipalidad de Paiján</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Slick Slider CSS -->
        <link href="css/slick-theme.css" rel="stylesheet"/>
        <!-- ICONS CSS -->
        <link href="css/font-awesome.css" rel="stylesheet">
		<!-- ICONS CSS -->
        <link href="css/animation.css" rel="stylesheet">
        <!-- Pretty Photo CSS -->
        <link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/jquery.bxslider.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/style5.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/demo.css" rel="stylesheet">
		<!-- Pretty Photo CSS -->
        <link href="css/fig-hover.css" rel="stylesheet">
        <!-- Typography CSS -->
        <link href="css/typography.css" rel="stylesheet">
        <!-- Custom Main StyleSheet CSS -->
        <link href="style.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/component.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/shotcode.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="css/sidebar-widget.css" rel="stylesheet">
		<!-- Custom Main StyleSheet CSS -->
        <link href="svg-icon.css" rel="stylesheet">
        <!-- Color CSS -->
        <link href="css/color.css" rel="stylesheet">
        <!-- Responsive CSS -->
        <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body class="demo-5">
        <!--WRAPPER START--> 
        <div class="wrapper"> 

			<?php include 'top.php'; ?>
			
			<!-- SAB BANNER START-->
			<div class="sab_banner overlay">
				<div class="container">
					<div class="sab_banner_text">
						<h2>Paiján</h2>
						<ul class="breadcrumb">
						  <li class="breadcrumb-item"><a href="home.php">Home</a></li>
						  <li class="breadcrumb-item"><a href="paijan.php">Paiján</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- SAB BANNER END-->
			<div class="row" style="padding:40px;"></div>
			<!-- CITY SERVICES2 WRAP START-->
			<!--<div class="city_health_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="city_health_text">
								<h2><span>Health and Social</span> Welfare</h2>
								<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh </p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="city_health_fig">
								<figure class="box">
									<div class="box-layer layer-1"></div>
									<div class="box-layer layer-2"></div>
									<div class="box-layer layer-3"></div>
									<img src="extra-images/health-fig.jpg" alt="">
								</figure>
							</div>
						</div>
					</div>
				</div>	
			</div>-->
			<!-- CITY SERVICES2 WRAP END-->
			
			
			<!-- CITY SERVICES2 WRAP START-->
			<div class="city_service_detail_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<div class="sidebar_widget">
								<!-- CITY SERVICE TABS START-->
								<div class="city_service_tabs tabs">
									<ul class="tab-links">
										<li class="fontazul"><a href="#tab1"> HISTORIA</a></li>
                                        <li><a href="#tab2">Escudo</a></li>
                                        <li><a href="#tab3">Himno</a></li>
                                        <li class="active"><a href="#tab1">Historia</a></li>
                                        <li><a href="#tab4">Población</a></li>
										<li class="fontazul"><a href="#tab5">UBICACIÓN</a></li>
										<li><a href="#tab5">Geografía y clima</a></li>
										<li><a href="#tab6">Mapa</a></li>
										<li class="fontazul"><a href="#tab7">CIUDAD SEGURA</a></li>
                                        <li><a href="#tab7">Comisarías de Paiján</a></li>
                                        <li><a href="#tab8">Télefonos de emergencia</a></li>
										<li class="fontazul"><a href="#tab9">COMITÉ DISTRITAL DE DE SEGURIDAD CIUDADANA</a></li>
                                        <li><a href="#tab9">CDSC 2021</a></li>
                                        <li><a href="#tab10">CDSC 2020</a></li>
                                        <li><a href="#tab11">CDSC 2019</a></li>
                                    </ul>
								</div>
								<!-- CITY SERVICE TABS END-->
								
								<!-- CITY SIDE INFO START-->
								<div class="city_side_info">
									<span><i class="fa fa-question-circle"></i></span>
									<h4>Información de Contáctos</h4>
									<h6>908-879-5100 89, <br>Calle Grau #207 <br> Paijan</h6>
								</div>
								<!-- CITY SIDE INFO END-->
								
								<!-- CITY NOTICE START-->
								<div class="city_notice">
									<h4>Public Notice</h4>
									<p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit</p>
									<a class="theam_btn" href="#" tabindex="0">Download PDF</a>
								</div>
								<!-- CITY NOTICE END-->
							</div>
						</div>
						<div class="col-md-9">
							<div class="tabs">
								<div class="tab-content">
									<div id="tab1" class="tab active">
                                        <div class="city_service_tabs_list">
											<!--<figure class="box">
												<div class="box-layer layer-1"></div>
												<div class="box-layer layer-2"></div>
												<div class="box-layer layer-3"></div>
												<img src="extra-images/service-tabs.jpg" alt="">
											</figure>-->
											<div class="city_service_tabs_text">
												<h3>Historia</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
									<div id="tab2" class="tab">
                                        <div class="city_service_tabs_list">										
									        <div class="city_service_tabs_text">
												<h3>Escudo</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
									<div id="tab3" class="tab">
									    <div class="city_service_tabs_list">										
									        <div class="city_service_tabs_text">
												<h3>Himno</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
									<div id="tab4" class="tab">
                                        <div class="city_service_tabs_list">										
									        <div class="city_service_tabs_text">
												<h3>Población</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
									<div id="tab5" class="tab">
                                        <div class="city_service_tabs_list">										
									        <div class="city_service_tabs_text">
												<h3>Geografía y clima</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
									<div id="tab6" class="tab">
                                        <div class="city_service_tabs_list">										
									        <div class="city_service_tabs_text">
												<h3>Mapa</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
                                    <div id="tab7" class="tab">
                                        <div class="city_service_tabs_list">										
									        <div class="city_service_tabs_text">
												<h3>Comisarías de Paiján</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
                                    <div id="tab8" class="tab">
                                        <div class="city_service_tabs_list">										
									        <div class="city_service_tabs_text">
												<h3>Teléfonos de emergencia</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
                                    <div id="tab9" class="tab">
                                        <div class="city_service_tabs_list">										
									        <div class="city_service_tabs_text">
												<h3>Comité distrital de seguridad ciudadana 2021</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
                                    <div id="tab10" class="tab">
                                        <div class="city_service_tabs_list">										
									        <div class="city_service_tabs_text">
												<h3>Comité distrital de seguridad ciudadana 2020</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
                                    <div id="tab11" class="tab">
                                        <div class="city_service_tabs_list">										
									        <div class="city_service_tabs_text">
												<h3>Comité distrital de seguridad ciudadana 2019</h3>
												<p>competitividad. Su organización responde a la generación de valor público, con innovación, creatividad, transparencia y sentido de urgencia; así como es un canal inmediato de participación vecinal e inversión público privada.</p>	
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- CITY DEPARTMENT LIST START-->
							<!--<div class="city_department_list">
								<ul>
									<li>
										<div class="city_department2_fig">
											<figure class="box">
												<div class="box-layer layer-1"></div>
												<div class="box-layer layer-2"></div>
												<div class="box-layer layer-3"></div>
												<img src="extra-images/department-01.jpg" alt="">
											</figure>
											<div class="city_department2_text">
												<h5>Fire Department</h5>
												<p>Poin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio...</p>
											</div>
										</div>
									</li>
									<li>
										<div class="city_department2_fig">
											<div class="city_department2_text text2">
												<h5>Police Department</h5>
												<p>Poin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio....</p>
											</div>
											<figure class="box">
												<div class="box-layer layer-1"></div>
												<div class="box-layer layer-2"></div>
												<div class="box-layer layer-3"></div>
												<img src="extra-images/department-02.jpg" alt="">
											</figure>
										</div>
									</li>
									<li>
										<div class="city_department2_fig">
											<figure class="box">
												<div class="box-layer layer-1"></div>
												<div class="box-layer layer-2"></div>
												<div class="box-layer layer-3"></div>
												<img src="extra-images/department-03.jpg" alt="">
											</figure>
											<div class="city_department2_text">
												<h5>Emergency Disaster Department</h5>
												<p>Poin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio...</p>
											</div>
										</div>
									</li>
								</ul>
							</div>-->
							<!-- CITY DEPARTMENT LIST END-->
							
							<!-- CITY EMERGENCY CALL START-->
							<div class="city_emergency_info">
								<div class="city_emergency_call">
									<h5>Important Telephone Numbers Energency</h5>
									<ul>
										<li><a href="#">Emergency</a></li>
										<li><a href="#">911</a></li>
										<li><a href="#">Fire Department</a></li>
										<li><a href="#">(949) 111 222 </a></li>
										<li><a href="#">Police Department</a></li>
										<li><a href="#">(949) 111 222 </a></li>
										<li><a href="#">Environmental Emergency</a></li>
										<li><a href="#">(949) 111 222 </a></li>
										<li><a href="#">Health Science Centre</a></li>
										<li><a href="#">(949) 111 222 </a></li>
										<li><a href="#">Children's Health and Rehabilitation Centre</a></li>
										<li><a href="#">(949) 111 222 </a></li>
									</ul>
								</div>
							</div>
							<!-- CITY EMERGENCY CALL END-->
						</div>
					</div>	
				</div>		
			</div>			
			<!--CITY REQUEST WRAP START-->
			<div class="city_requset_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-question"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Top Request</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="city_request_list">
								<div class="city_request_row">
									<span><i class="fa icon-shout"></i></span>
									<div class="city_request_text">
										<span>Recent</span>
										<h4>Announcement</h4>
									</div>
								</div>
								<div class="city_request_link">
									<ul>
										<li><a href="#">Pay a Parking Ticket</a></li>
										<li><a href="#">Building Violation</a></li>
										<li><a href="#">Affordable Housing</a></li>
										<li><a href="#">Graffiti Removal</a></li>
										<li><a href="#">Civil Service Exams</a></li>
										<li><a href="#">Rodent Baiting</a></li>
										<li class="margin0"><a href="#">Cleaning</a></li>
										<li class="margin0"><a href="#">Uncleared Sidewalk</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!--CITY REQUEST WRAP END-->
			
			<?php include 'footer.php'; ?>

		</div>
		 <!--WRAPPER END-->
        <!--Jquery Library-->
        <script src="js/jquery.js"></script>
    	<!--Bootstrap core JavaScript-->
        <script src="js/bootstrap.js"></script>
        <!--Slick Slider JavaScript-->
        <script src="js/slick.min.js"></script>
		<!--Pretty Photo JavaScript-->
        
        <!--Pretty Photo JavaScript-->
        <script src="js/jquery.prettyPhoto.js"></script>
		
		<!--Pretty Photo JavaScript-->	
        <script src="js/jquery.bxslider.min.js"></script>
		<!--Image Filterable JavaScript-->
		<script src="js/jquery-filterable.js"></script>
		<!--Pretty Photo JavaScript-->
        
		<!--Pretty Photo JavaScript-->
        <script src="js/modernizr.custom.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/jquery.dlmenu.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/downCount.js"></script>
		<!--Counter up JavaScript-->
        <script src="js/waypoints.js"></script>
		<!--Pretty Photo JavaScript-->
        <script src="js/waypoints-sticky.js"></script>
        
        <!--Custom JavaScript-->
    	<script src="js/custom.js"></script>
		<script>document.documentElement.className = 'js';</script>
    </body>
</html>